﻿#region

using System;
using System.Windows;
using System.Windows.Threading;

#endregion

namespace DxpSourceCodeTools {
    /// <summary>
    ///     App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application {

        protected override void OnStartup(StartupEventArgs e) {
            Current.DispatcherUnhandledException += App_OnDispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            base.OnStartup(e);
        }

        /// <summary>
        /// UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e) {
            try {
                Logger.AppLogger.AppendLog(e.Exception,"异常");
                e.Handled = true;
            } catch (Exception ex) {
                Logger.AppLogger.AppendLog(ex, "异常");
            }
        }

        /// <summary>
        /// 非UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            try {
                var exception = e.ExceptionObject as Exception;
                if (exception != null) {
                    Logger.AppLogger.AppendLog(exception,"异常:");
                }
            } catch (Exception ex) {
                Logger.AppLogger.AppendLog(ex,"异常:");
            }
        }
    }
}