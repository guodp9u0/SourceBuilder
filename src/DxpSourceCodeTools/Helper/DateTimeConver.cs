﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DxpSourceCodeTools {
    /// <summary>
    /// 自定义事件转换
    /// </summary>
    public class DateTimeConverter : IValueConverter {
        //当值从绑定源传播给绑定目标时，调用方法Convert
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return DependencyProperty.UnsetValue;
            DateTime date = (DateTime)value;
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
        //当值从绑定目标传播给绑定源时，调用此方法ConvertBack
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            string str = value as string;
            DateTime txtDate;
            if (DateTime.TryParse(str, out txtDate)) {
                return txtDate;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}