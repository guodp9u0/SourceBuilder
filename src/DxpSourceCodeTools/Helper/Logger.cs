﻿using System;
using System.Text;

namespace DxpSourceCodeTools {

    /// <summary>
    /// </summary>
    public class Logger {

        public event EventHandler<LoggerEventArgs> LogAppended;

        private Logger() {

        }

        public static Logger AppLogger = new Logger();

        protected virtual void OnLogAppended(LoggerEventArgs e) {
            LogAppended?.Invoke(this, e);
        }

        public void AppendLog(string message) {
            AppendLog(new LogEntry {
                Time= DateTime.Now,
                Message = message
            });
        }

        public void AppendLog(Exception ex,string message=null) {
            var sb = new StringBuilder();
            while (ex != null) {
                sb.AppendLine($"Message : {ex.Message}");
                sb.AppendLine($"StackTrace  : {ex.StackTrace}");
                ex = ex.InnerException;
            }
            AppendLog(new LogEntry {
                Type = LogType.Error,
                Time = DateTime.Now,
                Message = message,
                Detail = sb.ToString()
            });
        }

        private void AppendLog(LogEntry log) {
            FileLog.LogOperation("", log.Message);
            FileLog.LogOperation("", log.Detail);
            //todo
            //trigger events
            OnLogAppended(new LoggerEventArgs(log));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LoggerEventArgs : EventArgs {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public LoggerEventArgs(LogEntry log) {
            Log = log;
        }
        
        public LogEntry Log {
            get;
        }
    }

}