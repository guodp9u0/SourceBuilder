﻿#region

using System;
using System.IO;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    /// </summary>
    public class PathHelper {

        /// <summary>
        ///     计算第一个路径用相对于第二个路径的方式如何表示，例如
        ///     first   /A/B/C/D
        ///     second  /A/I/K
        ///     则会返回
        ///     ../../B/C/D
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static string CalculateRelativePath(string first, string second) {
            var firsts = first.Split(
                Path.DirectorySeparatorChar);
            var seconds = second.Split(
                Path.DirectorySeparatorChar);
            if (string.Equals(first, second)) {
                return Path.Combine(".", seconds[seconds.Length - 1]);
            }
            int minLen = Math.Min(firsts.Length, seconds.Length);
            int i = 0;
            for (; i < minLen; i++) {
                if (!string.Equals(firsts[i], seconds[i])) {
                    break;
                }
            }
            string path = "";
            if (i == seconds.Length) {
                // ./CCC/DDD/FF
                path = ".";
                for (int j = i; j < firsts.Length; j++) {
                    path = Path.Combine(path, firsts[j]);
                }
            } else {
                path = "..";
                for (int j = i; j < seconds.Length - 2; j++) {
                    path = Path.Combine(path, "..");
                }

                for (int j = i; j < firsts.Length; j++) {
                    path = Path.Combine(path, firsts[j]);
                }
            }
            return path;
        }

    }
}