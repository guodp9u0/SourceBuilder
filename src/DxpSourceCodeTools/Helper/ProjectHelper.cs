﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    /// </summary>
    public static class ProjectHelper {


        private static readonly string SlnName = "DevExpress.sln";

        private static readonly string NewProjectNamePrefix = "_";

        private static readonly Logger Logger = Logger.AppLogger;

        /// <summary>
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <returns></returns>
        public static IList<MyProject> LoadProjects(string sourceFolder) {
            Logger.AppendLog("Begin Load Projects");
            var projects = new List<MyProject>();
            DirectoryInfo info = new DirectoryInfo(sourceFolder);
            var projectFiles = info.GetFiles("*.csproj", SearchOption.AllDirectories);
            foreach (var projectFile in projectFiles) {
                if (projectFile.Name.StartsWith(NewProjectNamePrefix)) {
                    continue;
                }
                projects.Add(ParseProject(projectFile));
            }
            Logger.AppendLog("End Load Projects");
            return projects;
        }

        private static MyProject ParseProject(FileInfo projectFile) {
            var project = new XmlDocument();
            project.Load(projectFile.FullName);
            var guid = SelectProjectId(project);
            var name = SelectProjectAssemblyName(project);
            var projectReference = SelectProjectReferences(project);
            var references = SelectReferences(project);
            var myProject = new MyProject(
                guid,
                name,
                projectFile.DirectoryName,
                projectFile.Name,
                project,
                projectReference,
                references);
            Logger.AppLogger.AppendLog($"Project Loaded:{myProject.AssemblyName}");
            return myProject;
        }

        public static void SaveProjects(IList<MyProject> projects, Setting setting, string sourceFolder) {
            SaveProjectsAs(projects, setting);
            var slnName = SaveSolution(projects, sourceFolder);
        }

        private static string SaveSolution(IList<MyProject> projects, string sourceFolder) {
            return string.Empty;
        }

        private static void SaveProjectsAs(IList<MyProject> projects, Setting setting) {
            Dictionary<string, MyProject> dic1 = new Dictionary<string, MyProject>();
            foreach (var project in projects) {
                dic1.Add(project.AssemblyName, project);
            }
            foreach (MyProject project in projects) {
                SaveProjectAs(project, setting, dic1);
            }
        }

        private static void SaveProjectAs(MyProject project, Setting setting, Dictionary<string, MyProject> dic) {
            if (setting.ProjectReference) {
                if (project.References.Count != 0) {
                    var groupNode = CreateItemGroup(project.ProjectDoc);
                    foreach (var projectRef in project.References) {
                        if (dic.TryGetValue(projectRef.Include, out MyProject targetProject)) {
                            groupNode.AppendChild(CreateProjectReferenceNode(
                                project.ProjectDoc,
                                PathHelper.CalculateRelativePath(
                                Path.Combine(targetProject.DirectoryName, NewProjectNamePrefix + targetProject.FileName), 
                                Path.Combine(project.DirectoryName, NewProjectNamePrefix + project.FileName)),
                                targetProject.ProjectGuid,
                                targetProject.AssemblyName));
                            var node =SelectReferenceNode(projectRef.Include, project.ProjectDoc);
                            node.ParentNode.RemoveChild(node);
                        }
                    }
                    project.ProjectDoc.DocumentElement.AppendChild(groupNode);
                }
            }
            project.ProjectDoc.Save(Path.Combine(project.DirectoryName, NewProjectNamePrefix + project.FileName));
        }

        private static XmlNode CreateItemGroup(XmlDocument doc) {
            return doc.CreateNode(XmlNodeType.Element, "ItemGroup", "http://schemas.microsoft.com/developer/msbuild/2003");
        }

        private static XmlNode CreateProjectReferenceNode(XmlDocument doc, string include, string projectId, string name) {
            var node = doc.CreateNode(XmlNodeType.Element, "ProjectReference", "http://schemas.microsoft.com/developer/msbuild/2003");
            var includeAttribute = doc.CreateAttribute("Include");
            includeAttribute.Value = include;
            var projectNode = doc.CreateNode(XmlNodeType.Element, "Project", "http://schemas.microsoft.com/developer/msbuild/2003");
            projectNode.InnerText = projectId;
            var nameNode = doc.CreateNode(XmlNodeType.Element, "Name", "http://schemas.microsoft.com/developer/msbuild/2003");
            nameNode.InnerText = name;
            node.AppendChild(projectNode);
            node.AppendChild(nameNode);
            node.Attributes.Append(includeAttribute);
            return node;
        }

        public static void ComplieProjects(IList<MyProject> projects, Setting setting, string sourceFolder) {
            SortedList<int, MyProject> sortedProjects = new SortedList<int, MyProject>();
            Dictionary<string, int> dic1 = new Dictionary<string, int>();
            Dictionary<string, MyProject> dic2 = new Dictionary<string, MyProject>();
            foreach (var project in projects) {
                dic1[project.AssemblyName] = 0;
                dic2[project.AssemblyName] = project;
            }
            foreach (var keyValues in dic2) {
                var sqeuence = GetSequence(keyValues.Key, dic1, dic2);
                while (sortedProjects.ContainsKey(sqeuence)) {
                    sqeuence++;
                }
                sortedProjects.Add(sqeuence, dic2[keyValues.Key]);
            }
            foreach (var project in sortedProjects) {
                BuildProject(project.Value, setting, sourceFolder);
            }
        }

        private static int GetSequence(string myProject, Dictionary<string, int> dic1,
            Dictionary<string, MyProject> dic2) {
            if (!dic1.ContainsKey(myProject)) {
                Logger.AppLogger.AppendLog($"{myProject} Not Exist");
                return 0;
            }
            if (dic1[myProject] > 0) {

            } else {
                var references = dic2[myProject].References;
                if (references.Count == 0) {
                    dic1[myProject] = 1000;
                } else {
                    int max = 0;
                    foreach (var reference in dic2[myProject].References) {
                        max = GetSequence(reference.Include, dic1, dic2);
                    }
                    dic1[myProject] = max + 1000;
                }
            }
            return dic1[myProject];
        }


        private static void BuildProject(MyProject project, Setting setting, string sourceFolder) {
            Logger.AppLogger.AppendLog($"Begin Build {project.FileName}");
            //var pro = new Microsoft.Build.Evaluation.Project(project.FullFileName);
            //try {
            //    Logger.AppLogger.AppendLog($"End Build {project.FileName} : {pro.Build()}");;
            //} catch (Exception e) {
            //    Logger.AppLogger.AppendLog(e);
            //}
            string command = string.Concat(
                "\"", ToolsHelper.MsBuildPath,
                "\" /nologo /verbosity:quiet /p:Configuration=",
                setting.Configuration, ";Platform=AnyCPU \"",
                project.FullFileName, "\"");
            string result = ToolsHelper.ExecuteCommand(command, project.DirectoryName);
            Logger.AppLogger.AppendLog(command);
            Logger.AppLogger.AppendLog(result);
        }

        public static void RegistGac(Setting setting) {

        }

        public static void CleanGac(Setting setting) {

        }

        private static XmlNamespaceManager CreateXmlNamespaceManager(XmlDocument doc) {
            var xmlnsm = new XmlNamespaceManager(doc.NameTable);
            xmlnsm.AddNamespace("m", "http://schemas.microsoft.com/developer/msbuild/2003");
            return xmlnsm;
        }

        private static string SelectProjectId(XmlDocument doc) {
            var xmlnsm = CreateXmlNamespaceManager(doc);
            var node = doc.SelectSingleNode("/m:Project/m:PropertyGroup/m:ProjectGuid", xmlnsm);
            return node?.InnerText;
        }

        private static string SelectProjectAssemblyName(XmlDocument doc) {
            var xmlnsm = CreateXmlNamespaceManager(doc);
            var node = doc.SelectSingleNode("/m:Project/m:PropertyGroup/m:AssemblyName", xmlnsm);
            return node?.InnerText;
        }

        private static List<Reference> SelectReferences(XmlDocument doc) {
            var xmlnsm = CreateXmlNamespaceManager(doc);
            var references = new List<Reference>();
            var nodes = doc.SelectNodes("/m:Project/m:ItemGroup/m:Reference", xmlnsm);
            if (nodes != null) {
                foreach (XmlNode node in nodes) {
                    if (node.Attributes?["Include"] != null
                        && node.Attributes["Include"].InnerText.StartsWith("DevExpress")) {
                        var reference = new Reference {
                            Include = node.Attributes["Include"]?.InnerText,
                            HintPath = node["HintPath"]?.InnerText
                        };
                        references.Add(reference);
                    }
                }
            }
            return references;
        }

        private static XmlNode SelectReferenceNode(string refInclude, XmlDocument doc) {
            var xmlnsm = CreateXmlNamespaceManager(doc);
            var references = new List<Reference>();
            var nodes = doc.SelectNodes("/m:Project/m:ItemGroup/m:Reference", xmlnsm);
            if (nodes != null) {
                foreach (XmlNode node in nodes) {
                    if (node.Attributes?["Include"] != null
                        && node.Attributes["Include"].InnerText.Equals(refInclude)) {
                        return node;
                    }
                }
            }
            return null;
        }
        private static List<ProjectReference> SelectProjectReferences(XmlDocument doc) {
            var xmlnsm = CreateXmlNamespaceManager(doc);
            var references = new List<ProjectReference>();
            var nodes = doc.SelectNodes("/m:Project/m:ItemGroup/m:ProjectReference", xmlnsm);
            if (nodes != null) {
                foreach (XmlNode node in nodes) {
                    if (node.Attributes != null) {
                        var reference = new ProjectReference {
                            Project = node["Project"]?.InnerText,
                            Include = node.Attributes["Include"]?.InnerText,
                            Name = node["Name"]?.InnerText
                        };
                        references.Add(reference);
                    }
                }
            }
            return references;
        }
    }
}