﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Build.Utilities;

namespace DxpSourceCodeTools {

    public class ToolsHelper {

        public static void LogTest() {
            Logger.AppLogger.AppendLog(SnPath);
            Logger.AppLogger.AppendLog(GacUtilPath);
            Logger.AppLogger.AppendLog(MsBuildPath);
        }

        public static readonly string SnKeyDirectoryName = "DevExpress.Key";

        public static readonly string SnKeyFileName = "StrongKey.snk";

        public static readonly string SnPath = ToolLocationHelper.GetPathToDotNetFrameworkSdkFile(
        "sn.exe", TargetDotNetFrameworkVersion.VersionLatest);

        public static readonly string GacUtilPath = ToolLocationHelper.GetPathToDotNetFrameworkSdkFile(
        "gacutil.exe", TargetDotNetFrameworkVersion.VersionLatest);

        public static readonly string MsBuildPath = ToolLocationHelper.GetPathToDotNetFrameworkFile(
        "MSBuild.exe", TargetDotNetFrameworkVersion.VersionLatest);

        public static string BuildKeyFile(string sourceFolder) {
            string sNKeyPath = GetSnKeyPath(sourceFolder);
            string command = string.Concat("\"", SnPath, "\" -k \"", sNKeyPath, "\"");
            string text = ExecuteCommand(command, sourceFolder);
            if (File.Exists(sNKeyPath)) {
                Logger.AppLogger.AppendLog(text);
                Logger.AppLogger.AppendLog("Successfully generating strong key file");
            } else {
                throw new FileNotFoundException("Error during generating strong key file:"+
                    sNKeyPath+"\n"+ text);
            }
            return sNKeyPath;
        }

        private static string GetSnKeyPath(string sourceFolder) {
            return Path.Combine(sourceFolder,SnKeyDirectoryName, SnKeyFileName);
        }

        private static Encoding GetOemEncoding() {
            Encoding result;
            try {
                result = Encoding.GetEncoding(UnsafeNativeMethods.GetCpInfoExWSafe().CodePage);
            } catch (InvalidOperationException ex) {
                Logger.AppLogger.AppendLog(ex);
                result = Encoding.Default;
            }
            return result;
        }

        public static string ExecuteCommand(string command, string folder) {
            Process process = new Process {
                StartInfo = new ProcessStartInfo {
                    FileName = "cmd",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    Verb = "runas",
                    WorkingDirectory = folder,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    StandardOutputEncoding = GetOemEncoding()
                }
            };
            process.Start();
            StreamReader standardOutput = process.StandardOutput;
            StreamWriter standardInput = process.StandardInput;
            standardInput.WriteLine(command);
            standardInput.WriteLine("exit");
            string result = standardOutput.ReadToEnd();
            standardInput.Close();
            standardOutput.Close();
            return result;
        }

        private static PublicKeys GetPublicKeyToken(string snkPath) {
            bool flag = !File.Exists(snkPath);
            PublicKeys result;
            if (flag) {
                result = null;
            } else {
                FileStream fileStream = File.Open(snkPath, FileMode.Open, FileAccess.Read);
                StrongNameKeyPair strongNameKeyPair = new StrongNameKeyPair(fileStream);
                string publicKeyTokenName = GetPublicKeyTokenName(strongNameKeyPair);
                string publicKeyToken = GetPublicKeyToken(strongNameKeyPair);
                Logger.AppLogger.AppendLog($"Public key token Name: {publicKeyTokenName}");
                Logger.AppLogger.AppendLog($"Public key token: {publicKeyToken}");
                PublicKeys publicKeys = new PublicKeys(publicKeyTokenName, publicKeyToken);
                fileStream.Close();
                result = publicKeys;
            }
            return result;
        }

        private static string GetPublicKeyTokenName(StrongNameKeyPair k) {
            return ByteArrayToString(GetPublicKeyToken(k.PublicKey));
        }

        private static string GetPublicKeyToken(StrongNameKeyPair snkPair) {
            return ByteArrayToString(snkPair.PublicKey);
        }

        private static string ByteArrayToString(byte[] ba) {
            string text = BitConverter.ToString(ba);
            return text.Replace("-", "").ToLower();
        }

        private static byte[] GetPublicKeyToken(byte[] publicKey) {
            byte[] result;
            using (SHA1CryptoServiceProvider sHa1CryptoServiceProvider = new SHA1CryptoServiceProvider()) {
                byte[] array = sHa1CryptoServiceProvider.ComputeHash(publicKey);
                byte[] array2 = new byte[8];
                for (int i = 0; i < 8; i++) {
                    array2[i] = array[array.Length - i - 1];
                }
                result = array2;
            }
            return result;
        }

        public static string PatchInternalVisibleToContent(string sourceFolder,string fileContent) {
            var publicKey = GetPublicKeyToken(GetSnKeyPath(sourceFolder));
            return fileContent.Replace(PublicKeyToken, publicKey.PublicKeyToken);
        }

        public static readonly string PublicKeyTokenName = "b88d1754d700e49a";

        public static readonly string PublicKeyToken =
                "0024000004800000940000000602000000240000525341310004000001000100dfcd8cadc2dd24a7cd4ce95c4a9c1b8e7cb1dc2d665120556b4b0ec35495fddb2bd6eed0ca1e56480276295a225ba2a9746f3d3e1a04547ccf5b26acc3f96eb2a13ac467512497aa79208e32f242fd0618014d53c95a36e5de0e891873841fa8f559566e38e968426488b4aa4d0f0b59e59f38dcf3fbccf25d990ab19c27ddc2";
    }

    public class PublicKeys {

        public string PublicKeyName {
            get;
        }

        public string PublicKeyToken {
            get;
        }

        public PublicKeys(string publicKeyName, string publicKeyToken) {
            PublicKeyName = publicKeyName;
            PublicKeyToken = publicKeyToken;
        }
    }
}