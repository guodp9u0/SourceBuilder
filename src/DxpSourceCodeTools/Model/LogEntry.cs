﻿#region

using System;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    /// 
    /// </summary>
    public class LogEntry {

        /// <summary>
        /// 
        /// </summary>
        public LogEntry() {
            Type = LogType.Info;
        }

        /// <summary>
        /// 
        /// </summary>
        public LogType Type {
            get;
            set;
        }

        /// <summary>
        /// </summary>
        public DateTime Time {
            get;
            set;
        }

        /// <summary>
        /// </summary>
        public string Message {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Detail {
            get;
            set;
        }

    }
    public enum LogType {
        Info,
        Error
    }
}