﻿#region

#endregion

#region

using System.Collections.Generic;
using System.IO;
using System.Xml;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    ///     项目
    /// </summary>
    public class MyProject {

        /// <summary>
        /// </summary>
        /// <param name="projectGuid"></param>
        /// <param name="assemblyName"></param>
        /// <param name="directoryName"></param>
        /// <param name="fileName"></param>
        /// <param name="projectReferences"></param>
        /// <param name="references"></param>
        /// <param name="projectDoc"></param>
        public MyProject(string projectGuid
            , string assemblyName
            , string directoryName
            , string fileName
            , XmlDocument projectDoc
            , List<ProjectReference> projectReferences
            , List<Reference> references) {
            ProjectGuid = projectGuid;
            DirectoryName = directoryName;
            FileName = fileName;
            ProjectDoc = projectDoc;
            ProjectReferences = projectReferences;
            References = references;
            AssemblyName = assemblyName;
        }

        /// <summary>
        ///     项目编号
        /// </summary>
        public string ProjectGuid {
            get;
        }

        /// <summary>
        ///     项目在哪个文件夹下
        /// </summary>
        public string DirectoryName {
            get;
        }

        /// <summary>
        ///     cscproj 文件名
        /// </summary>
        public string FileName {
            get;
        }

        /// <summary>
        ///     cscproj 完整文件名
        /// </summary>
        public string FullFileName => Path.Combine(DirectoryName, FileName);

        /// <summary>
        ///     程序集名称
        /// </summary>
        public string AssemblyName {
            get;
        }

        /// <summary>
        /// </summary>
        public List<Reference> References {
            get;
        }

        /// <summary>
        /// </summary>
        public List<ProjectReference> ProjectReferences {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        public XmlDocument ProjectDoc {
            get;
        }

    }
}