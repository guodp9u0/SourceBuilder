﻿namespace DxpSourceCodeTools {

    /// <summary>
    ///     项目引用
    /// </summary>
    public class ProjectReference {

        /// <summary>
        /// </summary>
        public string Include {
            get;
            set;
        }

        /// <summary>
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// </summary>
        public string Project {
            get;
            set;
        }
    }
}