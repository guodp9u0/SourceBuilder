﻿namespace DxpSourceCodeTools {

    /// <summary>
    /// 普通引用
    /// </summary>
    public class Reference {

        /// <summary>
        /// 
        /// </summary>
        public string Include {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string HintPath {
            get;
            set;
        }
    }
}