﻿namespace DxpSourceCodeTools {

    /// <summary>
    ///     设置
    /// </summary>
    public class Setting {

        /// <summary>
        ///     编译配置
        /// </summary>
        public string Configuration {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool RemoveStrongKey {
            get;
            set;
        }

        /// <summary>
        ///     保存项目时候创建解决方案
        /// </summary>
        public bool CreateSolution {
            get;
            set;
        }

        /// <summary>
        ///     保存项目时候(尽可能)将引用更新为项目引用(在保存sln时候才会有效果)
        /// </summary>
        public bool ProjectReference {
            get;
            set;
        }

    }
}