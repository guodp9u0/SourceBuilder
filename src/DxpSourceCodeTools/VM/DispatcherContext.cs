﻿#region

using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Threading;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    /// 
    /// </summary>
    public sealed class DispatcherContext {

        private readonly Dispatcher _dispatcher;

        public bool IsSynchronized => _dispatcher.Thread == Thread.CurrentThread;

        public DispatcherContext() : this(Dispatcher.CurrentDispatcher) {
        }

        public DispatcherContext(Dispatcher dispatcher) {
            Debug.Assert(dispatcher != null);
            _dispatcher = dispatcher;
        }

        public void Invoke(Action action) {
            Debug.Assert(action != null);
            _dispatcher.Invoke(action);
        }

        public void BeginInvoke(Action action) {
            Debug.Assert(action != null);
            _dispatcher.BeginInvoke(action);
        }
    }
}