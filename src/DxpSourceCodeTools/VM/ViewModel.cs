﻿#region

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using DxpSourceCodeTools.Annotations;

#endregion

namespace DxpSourceCodeTools {

    /// <summary>
    /// </summary>
    public class ViewModel : INotifyPropertyChanged {

        /// <summary>
        /// </summary>
        public Setting Setting {
            get;
        }

        /// <summary>
        /// </summary>
        private readonly DispatcherContext _context;

        private readonly TaskFactory _taskFactory = new TaskFactory();

        private string _sourceCodePath;

        private bool _idle;

        private string _stringKeyPath;

        /// <summary>
        /// </summary>
        public ObservableCollection<LogEntry> Logs {
            get;
        }

        public ObservableCollection<MyProject> Projects {
            get;
        }

        public bool Idle {
            get => _idle;
            set {
                if (value == _idle) {
                    return;
                }
                _idle = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// </summary>
        public ViewModel() {
            _context = new DispatcherContext();
            Setting = new Setting {
                Configuration = "DEBUG",
                RemoveStrongKey = false,
                CreateSolution = true,
                ProjectReference = false
            };
            _idle = true;
            SourceCodePath = @"C:\Users\guodp\SourceCode\DXSourceCode\DXSource16.2.6";
            Logs = new ObservableCollection<LogEntry>();
            Projects = new ObservableCollection<MyProject>();
            Logger.AppLogger.LogAppended += AppLogger_LogAppended;
        }

        private void AppLogger_LogAppended(object sender, LoggerEventArgs e) {
            _context.BeginInvoke(
                () => {
                    Logs.Add(e.Log);
                });
        }

        public string SourceCodePath {
            get => _sourceCodePath;
            set {
                if (value == _sourceCodePath) {
                    return;
                }
                _sourceCodePath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// </summary>
        public void LoadProjects() {
            if (!Directory.Exists(SourceCodePath)) {
                throw new InvalidOperationException("Source Code Path Is Invalid");
            }
            StartBackGroundWork();
            _taskFactory.StartNew(
                () => {
                    try {
                        Projects.Clear();
                        var projects = ProjectHelper.LoadProjects(SourceCodePath);
                        foreach (var project in projects) {
                            _context.BeginInvoke(() => Projects.Add(project));
                        }
                    } finally {
                        EndBackGroundWork();
                    }
                });
        }

        public void Save() {
            StartBackGroundWork();
            _taskFactory.StartNew(
                () => {
                    try {
                        ProjectHelper.SaveProjects(Projects.ToList(), Setting, _sourceCodePath);
                    } finally {
                        EndBackGroundWork();
                    }
                });

        }


        public void Compile() {
            StartBackGroundWork();
            _taskFactory.StartNew(
                () => {
                    try {
                        ProjectHelper.ComplieProjects(Projects.ToList(), Setting, _sourceCodePath);
                    } finally {
                        EndBackGroundWork();
                    }

                });

        }

        public void GenerateKey() {
            _stringKeyPath = ToolsHelper.BuildKeyFile(SourceCodePath);
        }

        private void StartBackGroundWork() {
            if (!Idle) {
                throw new InvalidOperationException("another work started");
            }
            Idle = false;
        }

        private void EndBackGroundWork() {
            _context.BeginInvoke(() => Idle = true);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}