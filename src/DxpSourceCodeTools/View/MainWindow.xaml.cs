﻿#region

using System.Windows;

#endregion

namespace DxpSourceCodeTools {
    /// <summary>
    ///     MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow {

        private readonly ViewModel _viewModel;

        public MainWindow() {
            InitializeComponent();
            _viewModel = DataContext as ViewModel;
        }

        private void BtnSetting_Click(object sender, RoutedEventArgs e) {
            var window = new SettingWindow(_viewModel);
            window.ShowDialog();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e) {
            _viewModel.LoadProjects();
        }

        private void BtnLoadResult_Click(object sender, RoutedEventArgs e) {
            var window= new ProjectsWindow(_viewModel);
            window.ShowDialog();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e) {
            _viewModel.Save();
        }

        private void BtnCompile_Click(object sender, RoutedEventArgs e) {
            _viewModel.Compile();
        }

        private void BtnRegGac_Click(object sender, RoutedEventArgs e) {

        }

        private void BtnClearGac_Click(object sender, RoutedEventArgs e) {

        }

        private void BtnGenerate_Click(object sender, RoutedEventArgs e) {
            _viewModel.GenerateKey();
        }
    }
}