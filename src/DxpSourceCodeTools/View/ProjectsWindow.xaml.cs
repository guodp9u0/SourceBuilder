﻿#region

using System.Windows;

#endregion

namespace DxpSourceCodeTools {
    /// <summary>
    ///     ProjectsWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ProjectsWindow : Window {

        public ProjectsWindow(ViewModel viewModel) {
            InitializeComponent();
            DataGrid.ItemsSource = viewModel.Projects;
        }
    }
}