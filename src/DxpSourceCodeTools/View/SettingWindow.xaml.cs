﻿#region

using System.Windows;

#endregion

namespace DxpSourceCodeTools {
    /// <summary>
    ///     SettingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SettingWindow : Window {
        public SettingWindow(ViewModel viewModel) {
            InitializeComponent();
            DataContext = viewModel.Setting;
        }
    }
}