#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

#endregion

namespace SourceBuilderTool {
    public class ApplicationSettings {
        private List<string> _buildIgnoreProjects;

        private static ApplicationSettings _instance;

        public static ApplicationSettings Instance {
            get {
                bool flag = _instance == null;
                if (flag) {
                    _instance = new ApplicationSettings();
                }
                return _instance;
            }
        }

        public bool CommandPromptMode {
            get;
            set;
        }

        public bool ApplyPathInternalVisible {
            get;
            set;
        }

        public bool BuildMsBuildProjectFile {
            get;
            set;
        }

        public bool BuildProjects {
            get;
            set;
        }

        public bool AdminMode {
            get;
            set;
        }

        public bool RegisterToolbox {
            get;
            set;
        }

        public int WorkerProcessCount {
            get;
            set;
        }

        public string BuildConfiguration {
            get;
            set;
        }

        public Dxperience DxVersionToBuild {
            get;
            set;
        }

        public string KeyTokenPath {
            get;
            set;
        }

        public string PublicKeyToken {
            get;
            set;
        }

        public string PublicKeyName {
            get;
            set;
        }

        public List<string> BuildIgnoreProjects {
            get {
                bool flag = _buildIgnoreProjects == null;
                if (flag) {
                    _buildIgnoreProjects = new List<string>();
                }
                return _buildIgnoreProjects;
            }
            set {
                _buildIgnoreProjects = value;
            }
        }

        public AssembliesCollection AssembliesCollection {
            get;
            set;
        }

        public ApplicationSettings() {
            BuildConfiguration = "Release";
        }

        internal static void InitializeParameters(string[] inputParams) {
            Instance.AdminMode = false;
            bool flag = SystemSettings.Instance.DxVersionsInstalled.Count == 0;
            if (flag) {
                MessageBox.Show("No versions installed");
                Instance.DxVersionToBuild = null;
            } else {
                Instance.DxVersionToBuild =
                    SystemSettings.Instance.DxVersionsInstalled[SystemSettings.Instance.DxVersionsInstalled.Count - 1];
            }
            Instance.BuildProjects = true;
            Instance.RegisterToolbox = true;
            Instance.ApplyPathInternalVisible = true;
            Instance.BuildMsBuildProjectFile = false;
            Instance.CommandPromptMode = false;
            Instance.WorkerProcessCount = 4;
            bool flag2 = inputParams.Length != 0;
            if (flag2) {
                Instance.CommandPromptMode = true;
                Instance.BuildProjects = false;
                Instance.ApplyPathInternalVisible = false;
                for (int i = 0; i < inputParams.Length; i++) {
                    string text = inputParams[i];
                    string a = text;
                    if (a != "/b") {
                        if (a != "/t") {
                            if (a != "/p") {
                                if (a != "/s") {
                                    if (a != "/a") {
                                        bool flag3 = text.StartsWith("/buildIgnore:");
                                        if (flag3) {
                                            string text2 = text.Substring(
                                                text.IndexOf(":", StringComparison.CurrentCulture) + 1,
                                                text.Length - text.IndexOf(":", StringComparison.CurrentCulture) - 1);
                                            Instance.BuildIgnoreProjects.AddRange(text2.Split(';'));
                                        }
                                        bool flag4 = text.Contains("/v:");
                                        if (flag4) {
                                            string value = text.Substring(
                                                text.IndexOf(":", StringComparison.CurrentCulture) + 1,
                                                text.Length - text.IndexOf(":", StringComparison.CurrentCulture) - 1);
                                            foreach (Dxperience current in SystemSettings.Instance
                                                .DxVersionsInstalled) {
                                                bool flag5 = current.Version.Contains(value);
                                                if (flag5) {
                                                    Instance.DxVersionToBuild = current;
                                                }
                                            }
                                        }
                                        bool flag6 = text.Contains("/c:");
                                        if (flag6) {
                                            bool flag7 = text.Contains("Release");
                                            if (flag7) {
                                                Instance.BuildConfiguration = "Release";
                                            }
                                        }
                                        bool flag8 = text.Contains("/w:");
                                        if (flag8) {
                                            Instance.WorkerProcessCount = Convert.ToInt32(
                                                text.Substring(3, 1), CultureInfo.InvariantCulture);
                                            bool flag9 = text.Contains("Release");
                                            if (flag9) {
                                                Instance.BuildConfiguration = "Release";
                                            }
                                        }
                                    } else {
                                        Instance.AdminMode = true;
                                        Instance.BuildProjects = true;
                                        Instance.ApplyPathInternalVisible = true;
                                        Instance.CommandPromptMode = false;
                                    }
                                } else {
                                    Instance.ApplyPathInternalVisible = true;
                                    Instance.BuildMsBuildProjectFile = true;
                                }
                            } else {
                                Instance.ApplyPathInternalVisible = true;
                            }
                        } else {
                            Instance.RegisterToolbox = false;
                        }
                    } else {
                        Instance.BuildProjects = true;
                        Instance.ApplyPathInternalVisible = true;
                    }
                }
            }
        }
    }
}