#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using SourceBuilderTool.BaseClasses;
using SourceBuilderTool.Builder;

#endregion

namespace SourceBuilderTool {
    public class ApplicationWorkflow {
        private DateTime _start = DateTime.MinValue;

        private static ApplicationWorkflow _instance;

        private List<string> _assembliesToRemoveAfterCompilatilation = new List<string>();

        public bool BuildInProgress {
            get;
            set;
        }

        public bool IsBuildPossible {
            get;
            set;
        }

        public IList<Project> LoadedProjects {
            get;
            set;
        }

        private SystemSettings SystemSettings {
            get {
                return SystemSettings.Instance;
            }
        }

        private ApplicationSettings ApplicationSettings {
            get {
                return ApplicationSettings.Instance;
            }
        }

        public static ApplicationWorkflow Instance {
            get {
                bool flag = _instance == null;
                if (flag) {
                    _instance = new ApplicationWorkflow();
                }
                return _instance;
            }
        }

        public void Run() {
            MachineConfigCheck.CheckSystemErrorInfo(SystemSettings);
            bool flag = !ApplicationSettings.CommandPromptMode;
            if (flag) {
                MainForm.Instance.ShowDialog();
            } else {
                StartBuildingProcess();
            }
        }

        public void StartBuildingProcess() {
            bool isBuildPossible = IsBuildPossible;
            if (isBuildPossible) {
                BuildInProgress = true;
                _start = DateTime.Now;
                PrepareStrongKeyNameFile();
                Logger.Instance.AddText(
                    string.Format(
                        "Start building. Version={0}, Configuration={1}", ApplicationSettings.DxVersionToBuild.Version,
                        ApplicationSettings.BuildConfiguration), TextItemStatus.Info);
                RemoveDesignTimeExtensions();
                LoadAllProjects();
                bool applyPathInternalVisible = ApplicationSettings.ApplyPathInternalVisible;
                if (applyPathInternalVisible) {
                    ApplyPatch();
                }
                bool buildProjects = ApplicationSettings.BuildProjects;
                if (buildProjects) {
                    bool registerToolbox = ApplicationSettings.RegisterToolbox;
                    if (registerToolbox) {
                        DeleteToolboxItems();
                    }
                    ClearGac();
                    string text = Path.Combine(ApplicationSettings.DxVersionToBuild.RootFolder, "Bin");
                    string outputPath = Path.Combine(text, "Framework");
                    FileOperations.DeleteAllAssembliesInTheFolder(text);
                    bool flag = FileOperations.LockedFiles.Count > 0;
                    if (flag) {
                        Logger.Instance.AddText(
                            "Locked files found. It is recommended to reboot the machine and try building sources again",
                            TextItemStatus.Error);
                    }
                    Logger.Instance.SuccessfullyBuildProjectCount = 0;
                    BackgroundBuilder backgroundBuilder = new BackgroundBuilder();
                    bool flag2 = !MachineConfigCheck.CheckForInternetConnection()
                                 && DxBuildVersionHelper.IsVersion16AndHigher(
                                     ApplicationSettings.Instance.DxVersionToBuild.Version);
                    if (flag2) {
                        MessageBox.Show(
                            string.Format(
                                Strings.NoInternetConnection,
                                Path.Combine(ApplicationSettings.DxVersionToBuild.RootFolder, "Bin")),
                            Strings.NoInternetConnectionCaption);
                    } else {
                        _assembliesToRemoveAfterCompilatilation.AddRange(
                            NuGetHelper.GetRequiredAssembliesFromNuGet(
                                ApplicationSettings.DxVersionToBuild.RootFolder, outputPath));
                    }
                    RestoreEntityFrameworkAssemblies();
                    backgroundBuilder.BuildProjects(LoadedProjects);
                } else {
                    FinalizingApplication(null);
                }
            }
            bool buildMsBuildProjectFile = ApplicationSettings.BuildMsBuildProjectFile;
            if (buildMsBuildProjectFile) {
                MsBuildProjGenerator.ProcessProjects(LoadedProjects);
            }
        }

        public void PrepareStrongKeyNameFile() {
            bool flag = File.Exists(SystemSettings.BuildSettings.SnPath);
            if (flag) {
                bool flag2 = !PreparePublicKey();
                if (flag2) {
                    BuildInProgress = false;
                    MainForm.Instance.EnableEditors();
                }
            } else {
                Logger.Instance.AddText("Sn tool is not found, public key part", TextItemStatus.Error);
            }
        }

        private void RemoveDesignTimeExtensions() {
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += Worker_DoWork;
            Logger.Instance.AddText("Removing design time extensions", TextItemStatus.Info);
            backgroundWorker.RunWorkerAsync();
            while (!backgroundWorker.IsBusy) {
                Application.DoEvents();
            }
            Logger.Instance.AddText("Done removing design time extensions", TextItemStatus.Info);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            DesignExtensionsManager.UninstallWinExtensions(
                ApplicationSettings.DxVersionToBuild.RootFolder, ApplicationSettings.DxVersionToBuild.Version);
            DesignExtensionsManager.UninstallXafExtensions(
                SystemSettings.ProgramFilesFolder, ApplicationSettings.DxVersionToBuild.RootFolder,
                ApplicationSettings.DxVersionToBuild.Version);
        }

        private void RestoreEntityFrameworkAssemblies() {
            string path = "Sources\\DevExpress.DemoData\\DevExpress.DemoData.Core\\Dlls";
            string text = Path.Combine(ApplicationSettings.DxVersionToBuild.RootFolder, path);
            string path2 = Path.Combine(ApplicationSettings.DxVersionToBuild.RootFolder, "Bin", "Framework");
            bool flag = Directory.Exists(text);
            if (flag) {
                string[] files = Directory.GetFiles(text);
                for (int i = 0; i < files.Length; i++) {
                    string text2 = files[i];
                    string text3 = Path.Combine(path2, Path.GetFileName(text2));
                    File.Copy(text2, text3, true);
                    bool flag2 = File.Exists(text3);
                    if (flag2) {
                        _assembliesToRemoveAfterCompilatilation.Add(text3);
                    }
                }
                Logger.Instance.AddText("EntityFramework assemblies restored", TextItemStatus.Info);
            } else {
                Logger.Instance.AddText("Directory " + text + " does not exist", TextItemStatus.Error);
            }
        }

        private void ApplyPatch() {
            FixDevExpressBuildtargetsIssue();
            foreach (Project current in LoadedProjects) {
                ProjectFileUpdater.ApplyDebugInfoPatch(current.FullPath);
                string path = Directory.GetParent(current.FullPath).ToString();
                string[] files = Directory.GetFiles(path, "AssemblyVersion.cs", SearchOption.AllDirectories);
                string[] array = files;
                for (int i = 0; i < array.Length; i++) {
                    string file = array[i];
                    ProjectFileUpdater.ApplyPatchInternalVisibleToFiles(file, ApplicationSettings.PublicKeyName);
                }
                files = Directory.GetFiles(path, "AssemblyInfo.cs", SearchOption.AllDirectories);
                string[] array2 = files;
                for (int j = 0; j < array2.Length; j++) {
                    string file2 = array2[j];
                    ProjectFileUpdater.ApplyPatchInternalVisibleToFiles(file2, ApplicationSettings.PublicKeyName);
                }
            }
        }

        private void FixDevExpressBuildtargetsIssue() {
            Directory.GetDirectoryRoot(ApplicationSettings.DxVersionToBuild.RootFolder);
            bool flag = File.Exists(
                ApplicationSettings.DxVersionToBuild.SourceFolder + "SDK\\DevExpress.Build.targets");
            if (flag) {
                string fullName = Directory.GetParent(ApplicationSettings.DxVersionToBuild.RootFolder).Parent.FullName;
                Directory.CreateDirectory(fullName + "\\Bin\\WinRT\\");
                File.Copy(
                    ApplicationSettings.DxVersionToBuild.SourceFolder + "SDK\\DevExpress.Build.targets",
                    fullName + "\\Bin\\WinRT\\DevExpress.Build.targets", true);
            }
        }

        public void ClearGac() {
            string[] directories = Directory.GetDirectories(
                SystemSettings.GacFullPath, "DevExpress*" + ApplicationSettings.DxVersionToBuild.Version + "*",
                SearchOption.AllDirectories);
            bool flag = directories.Length != 0;
            if (flag) {
                string[] array = directories;
                for (int i = 0; i < array.Length; i++) {
                    string text = array[i];
                    bool flag2 = !Exclusions.IsAssemblyNotToDeleteFromGac(text);
                    if (flag2) {
                        FileOperations.DeleteFolder(text);
                    }
                }
            }
            directories = Directory.GetDirectories(
                SystemSettings.Gac4FullPath, "DevExpress*" + ApplicationSettings.DxVersionToBuild.Version + "*",
                SearchOption.AllDirectories);
            bool flag3 = directories.Length != 0;
            if (flag3) {
                string[] array2 = directories;
                for (int j = 0; j < array2.Length; j++) {
                    string text2 = array2[j];
                    bool flag4 = !Exclusions.IsAssemblyNotToDeleteFromGac(text2);
                    if (flag4) {
                        FileOperations.DeleteFolder(text2);
                    }
                }
            }
        }

        public void FinalizingApplication(IList<Project> projects) {
            BuildWinRTviaBatFile();
            BuildUwPviaBatFile();
            bool registerToolbox = ApplicationSettings.RegisterToolbox;
            if (registerToolbox) {
                UpdateToolboxItems();
            }
            bool flag = !ApplicationSettings.CommandPromptMode;
            if (flag) {
                MainForm.Instance.EnableEditors();
            }
            foreach (Project current in projects) {
                Logger.Instance.AddText(
                    string.Format("Project was not build: {0}", current.AssemblyName), TextItemStatus.Warning);
            }
            Logger.Instance.ProgressValue = Logger.Instance.MaxProgressValue;
            BuildInProgress = false;
            Logger.Instance.AddText("Build is finished.Running time: " + (DateTime.Now - _start), TextItemStatus.Info);
            Logger.SaveToFile("build.log");
            Logger.SaveErrorsToFile("build_errors.log");
            Logger.Instance.AddText("Build log was saved to build.log file", TextItemStatus.Info);
            bool commandPromptMode = ApplicationSettings.CommandPromptMode;
            if (commandPromptMode) {
                Logger.Instance.AddText(
                    string.Format(
                        "MaxProgressValue = {0}, SuccessfullyBuildProjectCount = {1}", Logger.Instance.MaxProgressValue,
                        Logger.Instance.SuccessfullyBuildProjectCount), TextItemStatus.Error);
                Environment.Exit(Logger.Instance.MaxProgressValue - Logger.Instance.SuccessfullyBuildProjectCount);
            }
        }

        private void BuildUwPviaBatFile() {
            string text = Path.Combine(
                ApplicationSettings.Instance.DxVersionToBuild.SourceFolder, "BuildUWP", "buildUWP.bat");
            bool flag = File.Exists(text);
            if (flag) {
                Process.Start(text);
            }
        }

        private void BuildWinRTviaBatFile() {
            string path = ApplicationSettings.Instance.DxVersionToBuild.SourceFolder + "\\buildWinRT.bat";
            bool flag = File.Exists(path);
            if (flag) {
                string text = CommandPromptOperationHelper.ExecuteCommand(
                    "buildWinRT.bat", ApplicationSettings.Instance.DxVersionToBuild.SourceFolder);
                Logger.Instance.AddText(text, TextItemStatus.Info);
            }
        }

        public void LoadAllProjects() {
            Logger.Instance.AddText("Loading projects. Please wait...", TextItemStatus.Info);
            LoadedProjects = new List<Project>();
            ProjectsLoader.LoadProjects(LoadedProjects);
            ProjectsLoader.RemoveAssemblies(LoadedProjects);
            Logger.Instance.AddText(
                string.Format("Loaded Projects Count: {0}", LoadedProjects.Count), TextItemStatus.Info);
            ProjectListManager.InitializeProjectReferences(LoadedProjects);
            LoadedProjects = ProjectListManager.SortItems(LoadedProjects);
            ProjectListManager.CategorizeProjects(LoadedProjects);
            ProjectsLoader.DetectCrossReferences(LoadedProjects);
        }

        public bool PreparePublicKey() {
            bool flag = !StrongKeyManagerClass.IsKeyExists();
            bool result;
            if (flag) {
                bool commandPromptMode = ApplicationSettings.CommandPromptMode;
                if (commandPromptMode) {
                    StrongKeyManagerClass.GenerateRandomKey(
                        ApplicationSettings.DxVersionToBuild.SourceFolder, SystemSettings.BuildSettings.SnPath);
                } else {
                    StrongKeySelectionForm strongKeySelectionForm = new StrongKeySelectionForm();
                    strongKeySelectionForm.ShowDialog();
                    bool flag2 = strongKeySelectionForm.Operation == "random";
                    if (flag2) {
                        StrongKeyManagerClass.GenerateRandomKey(
                            ApplicationSettings.DxVersionToBuild.SourceFolder, SystemSettings.BuildSettings.SnPath);
                    } else {
                        bool flag3 = strongKeySelectionForm.Operation == "copy";
                        if (flag3) {
                            bool flag4 = !StrongKeyManagerClass.CopyKey();
                            if (flag4) {
                                result = false;
                                return result;
                            }
                        }
                    }
                }
            } else {
                ApplicationSettings.KeyTokenPath = Path.Combine(
                    ApplicationSettings.Instance.DxVersionToBuild.SourceFolder, "DevExpress.Key", "StrongKey.snk");
            }
            PublicKeys publicKeyToken = StrongNameKeyHelper.GetPublicKeyToken(ApplicationSettings.KeyTokenPath);
            ApplicationSettings.Instance.PublicKeyToken = publicKeyToken.PublicKeyToken;
            ApplicationSettings.Instance.PublicKeyName = publicKeyToken.PublicKeyName;
            result = true;
            return result;
        }

        public static void ProcessInputParameter(string[] inputParams) {
            ApplicationSettings.InitializeParameters(inputParams);
        }

        private void UpdateToolboxItems() {
            Logger.Instance.AddText("Creating Toolbox items...", TextItemStatus.Info);
            string text = "ToolboxCreator.exe /ini:ToolboxCreator.ini ";
            text = CommandPromptOperationHelper.ExecuteCommand(text, ApplicationSettings.DxVersionToBuild.RootFolder);
            FileOperations.SaveStringToFile(text, "build_ToolboxCreate.log");
            Logger.Instance.AddText("Toolbox items are created", TextItemStatus.Info);
            try {
                File.Delete(ApplicationSettings.DxVersionToBuild.RootFolder + "\\ToolboxCreator.exe");
                File.Delete(ApplicationSettings.DxVersionToBuild.RootFolder + "\\ToolboxCreator.ini");
            } catch {
                Logger.Instance.AddText(
                    "The " + ApplicationSettings.DxVersionToBuild.RootFolder
                    + "\\ToolboxCreator.exefile could not be deleted", TextItemStatus.Warning);
            }
        }

        private void DeleteToolboxItems() {
            File.Copy(
                ApplicationSettings.DxVersionToBuild.RootFolder + "Tools\\ToolboxCreator.exe",
                ApplicationSettings.DxVersionToBuild.RootFolder + "ToolboxCreator.exe", true);
            File.Copy(
                ApplicationSettings.DxVersionToBuild.RootFolder + "Tools\\ToolboxCreator.ini",
                ApplicationSettings.DxVersionToBuild.RootFolder + "ToolboxCreator.ini", true);
            Logger.Instance.AddText("Removing Toolbox items...", TextItemStatus.Info);
            string text = "ToolboxCreator.exe /ini:ToolboxCreator.ini /remove ";
            text = CommandPromptOperationHelper.ExecuteCommand(text, ApplicationSettings.DxVersionToBuild.RootFolder);
            FileOperations.SaveStringToFile(text, "build_ToolboxRemove.log");
            Logger.Instance.AddText("Toolbox items are removed", TextItemStatus.Info);
        }
    }
}