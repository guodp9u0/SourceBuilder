#region

using System.Collections.Generic;

#endregion

namespace SourceBuilderTool {
    public class AssembliesCollection {
        public List<string> AssembliesInOutputBeforeBuild {
            get;
            set;
        }

        public List<string> AssembliesInOutputAfterBuild {
            get;
            set;
        }

        public List<string> AssembliesInGacBeforeBuild {
            get;
            set;
        }

        public List<string> AssembliesInGacAfterBuild {
            get;
            set;
        }

        private AssembliesCollection() {
            AssembliesInGacAfterBuild = new List<string>();
            AssembliesInGacBeforeBuild = new List<string>();
            AssembliesInOutputAfterBuild = new List<string>();
            AssembliesInOutputBeforeBuild = new List<string>();
        }
    }
}