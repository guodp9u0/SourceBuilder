#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

#endregion

namespace SourceBuilderTool {
    internal class BackgroundBuilder {
        private List<ProjectBackgroundWorker> _workers;

        private IList<Project> _projects;

        private int _counter = 1;

        public void BuildProjects(IList<Project> loadedProjects) {
            Logger.Instance.MaxProgressValue = loadedProjects.Count;
            Logger.Instance.ProgressValue = 0;
            InitializeProjectBackgroundWorker();
            _projects = new List<Project>();
            foreach (Project current in loadedProjects) {
                _projects.Add(current);
            }
            FindAJobForFreeWorkers();
            bool commandPromptMode = ApplicationSettings.Instance.CommandPromptMode;
            if (commandPromptMode) {
                Console.Read();
            }
        }

        private void FindAJobForFreeWorkers() {
            foreach (ProjectBackgroundWorker current in _workers) {
                bool flag = !current.IsBusy && !current.Locked;
                if (flag) {
                    current.Locked = true;
                    Project project = CheckForProjectToRebuild();
                    bool flag2 = project != null;
                    if (flag2) {
                        current.Project = project;
                        project.Building = true;
                        Logger argB60 = Logger.Instance;
                        string[] expr_6D = new string[5];
                        int arg_8D1 = 0;
                        int num = _counter;
                        _counter = num + 1;
                        expr_6D[arg_8D1] = num.ToString(CultureInfo.InvariantCulture);
                        expr_6D[1] = " Building : ";
                        expr_6D[2] = project.AssemblyName;
                        expr_6D[3] = " ";
                        expr_6D[4] = project.TargetFrameworkIdentifier;
                        argB60.AddText(string.Concat(expr_6D), TextItemStatus.Info);
                    }
                }
            }
            foreach (ProjectBackgroundWorker current2 in _workers) {
                bool flag3 = !current2.IsBusy && current2.Project != null;
                if (flag3) {
                    current2.RunWorkerAsync(current2.Project.AssemblyName);
                }
            }
        }

        private Project CheckForProjectToRebuild() {
            Project result;
            for (int i = _projects.Count - 1; i >= 0; i--) {
                Project project = _projects[i];
                bool flag = project.IsProjectCouldBeCompiled && !project.IsRebuilt
                            && project.IsAllPredecessorsAlreadyReBuilt() && !project.Building;
                if (flag) {
                    result = project;
                    return result;
                }
            }
            result = null;
            return result;
        }

        private bool IsAllAssembliesWereRebuilt() {
            bool result;
            for (int i = 0; i < _projects.Count; i++) {
                bool flag = !_projects[i].IsRebuilt && _projects[i].IsProjectCouldBeCompiled;
                if (flag) {
                    result = false;
                    return result;
                }
            }
            bool flag2 = AllWorkersAreFree();
            if (flag2) {
                ApplicationWorkflow.Instance.FinalizingApplication(_projects);
            }
            result = true;
            return result;
        }

        private bool AllWorkersAreFree() {
            bool result;
            foreach (ProjectBackgroundWorker current in _workers) {
                bool isBusy = current.IsBusy;
                if (isBusy) {
                    result = false;
                    return result;
                }
            }
            result = true;
            return result;
        }

        private void InitializeProjectBackgroundWorker() {
            _workers = new List<ProjectBackgroundWorker>();
            for (int i = 0; i < 4; i++) {
                ProjectBackgroundWorker projectBackgroundWorker = new ProjectBackgroundWorker {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true,
                    Locked = false
                };
                _workers.Add(projectBackgroundWorker);
                projectBackgroundWorker.DoWork += worker_DoWork;
                projectBackgroundWorker.ProgressChanged += worker_ProgressChanged;
                projectBackgroundWorker.RunWorkerCompleted += worker_RunWorkerCompleted;
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            ProjectBackgroundWorker projectBackgroundWorker = sender as ProjectBackgroundWorker;
            projectBackgroundWorker.Locked = false;
            projectBackgroundWorker.Project.Building = false;
            projectBackgroundWorker.Project.IsRebuilt = true;
            _projects.Remove(projectBackgroundWorker.Project);
            projectBackgroundWorker.Project = null;
            Logger.Instance.ProgressValue = Logger.Instance.ProgressValue + 1;
            bool flag = !IsAllAssembliesWereRebuilt();
            if (flag) {
                FindAJobForFreeWorkers();
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            Logger.Instance.AddText(e.UserState.ToString(), (TextItemStatus) e.ProgressPercentage);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e) {
            ProjectBackgroundWorker projectBackgroundWorker = sender as ProjectBackgroundWorker;
            Project project = projectBackgroundWorker.Project;
            TextItem textItem = ProjectsBuilder.BuildProject(project);
            bool flag = textItem.Status == TextItemStatus.Success;
            if (flag) {
                Logger expr28 = Logger.Instance;
                int successfullyBuildProjectCount = expr28.SuccessfullyBuildProjectCount;
                expr28.SuccessfullyBuildProjectCount = successfullyBuildProjectCount + 1;
            }
            bool flag2 = !string.IsNullOrEmpty(textItem.Text);
            if (flag2) {
                projectBackgroundWorker.ReportProgress((int) textItem.Status, textItem.Text);
            }
            bool flag3 = project.Category != ProjectCategory.Silverlight || project.Category != ProjectCategory.WinRt;
            if (flag3) {
                bool flag4 = project.AssemblyFullPathAfterBuild != null;
                if (flag4) {
                    projectBackgroundWorker.ReportProgress(0, "Registering asembly in GAC: " + project.AssemblyName);
                    bool flag5 = ProjectsBuilder.RegisterInGac(project.AssemblyFullPathAfterBuild);
                    if (flag5) {
                        projectBackgroundWorker.ReportProgress(
                            3, project.AssemblyName + " has been registered in GAC successfuly");
                    } else {
                        projectBackgroundWorker.ReportProgress(2, project.AssemblyName + " is not registered in GAC");
                    }
                }
            }
        }
    }
}