#region

using System;

#endregion

namespace SourceBuilderTool.BaseClasses {
    public static class DxBuildVersionHelper {
        public static bool IsVersion15AndHigher(string version) {
            return IsVersionHigherThan(version, 15);
        }

        public static bool IsVersion16AndHigher(string version) {
            return IsVersionHigherThan(version, 16);
        }

        private static bool IsVersionHigherThan(string version, int value) {
            return Convert.ToInt32(version.TrimStart('v').Split('.')[0]) >= value;
        }
    }
}