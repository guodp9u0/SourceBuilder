#region

using System.IO;
using Microsoft.Build.Utilities;

#endregion

namespace SourceBuilderTool.BaseClasses {
    internal class DesignExtensionsManager {
        public static void UninstallWinExtensions(string rootFolder, string version) {
            string path = $"DevExpress.Win.Projects.{version}.Installers.dll";
            string arg = Path.Combine(rootFolder, "System", "Components", path);
            string pathToDotNetFrameworkFile = ToolLocationHelper.GetPathToDotNetFrameworkFile(
                "InstallUtil.exe", TargetDotNetFrameworkVersion.Version40);
            bool flag = !File.Exists(pathToDotNetFrameworkFile);
            if (flag) {
                Logger.Instance.AddText(pathToDotNetFrameworkFile + " does not exist", TextItemStatus.Error);
            } else {
                string format = "{0} /u /LogToConsole=false \"{1}\"";
                string text = string.Format(format, pathToDotNetFrameworkFile, arg);
                CommandPromptOperationHelper.ExecuteCommand("", "");
            }
        }

        public static void UninstallXafExtensions(string programFilesFolder, string rootFolder, string dxversion) {
            string[] array = {
                "14.0",
                "12.0",
                "11.0",
                "10.0"
            };
            string[] array2 = array;
            for (int i = 0; i < array2.Length; i++) {
                string arg = array2[i];
                string empty = string.Empty;
                string text = Path.Combine(
                    programFilesFolder, string.Format("Microsoft Visual Studio {0}", arg),
                    "Common7\\IDE\\VSIXInstaller.exe");
                bool flag = File.Exists(text);
                if (flag) {
                    string command = string.Format(
                        "{0} /uninstall:DevExpress.ExpressApp.Design.DynamicPackage.{1} /q", text, dxversion);
                    CommandPromptOperationHelper.ExecuteCommand(command, rootFolder);
                }
                bool flag2 = !string.IsNullOrEmpty(empty);
                if (flag2) {
                    Logger.Instance.AddText(string.Format("VSIXInstaller: {0}", empty), TextItemStatus.Info);
                }
            }
        }
    }
}