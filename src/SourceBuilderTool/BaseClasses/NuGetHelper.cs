#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;

#endregion

namespace SourceBuilderTool.BaseClasses {
    internal static class NuGetHelper {
        private static string _nuget = string.Empty;

        private static void DownloadNugetIfNecessary(string installPath) {
            bool flag = !File.Exists(_nuget);
            if (flag) {
                DownloadNuget(installPath);
            }
        }

        private static string[] RestoreNuGetPackage(string installPath, string outputPath, string folderToCopy,
            string package, string version) {
            DownloadNugetIfNecessary(installPath);
            List<string> list = new List<string>();
            InstallPackage(package, version, installPath);
            return CopyAssemblies(installPath, outputPath, folderToCopy);
        }

        public static string[] GetRequiredAssembliesFromNuGet(string installPath, string outputPath) {
            List<string> list = new List<string>();
            DownloadNuget(installPath);
            string path = Path.Combine(installPath, Strings.NugetPackagesInstallationPath);
            bool flag = Directory.Exists(path);
            if (flag) {
                Directory.Delete(path, true);
            }
            Directory.CreateDirectory(path);
            bool flag2 =
                DxBuildVersionHelper.IsVersion16AndHigher(ApplicationSettings.Instance.DxVersionToBuild.Version);
            if (flag2) {
                list.AddRange(RestoreMonoCecilAssemblies(installPath, outputPath));
                list.AddRange(RestoreNewtonsoftJsonAssembliy(installPath, outputPath));
                list.AddRange(RestoreDotlessCoreAssembliy(installPath, outputPath));
                list.AddRange(RestoreSharpZipLibAssembliy(installPath, outputPath));
            }
            bool flag3 =
                DxBuildVersionHelper.IsVersion15AndHigher(ApplicationSettings.Instance.DxVersionToBuild.Version);
            if (flag3) {
                list.AddRange(RestoreServiceBusAssembliy(installPath, outputPath));
                list.AddRange(RestoreAzureStorageAssembliy(installPath, outputPath));
            }
            RemovePackages(installPath);
            return list.ToArray();
        }

        public static string[] RestoreMonoCecilAssemblies(string installPath, string outputPath) {
            return RestoreNuGetPackage(
                installPath, outputPath, "Mono.Cecil.0.9.6.1\\lib\\net40", Strings.MonoCecil, "0.9.6.1");
        }

        public static string[] RestoreServiceBusAssembliy(string installPath, string outputPath) {
            return RestoreNuGetPackage(
                installPath, outputPath, "WindowsAzure.ServiceBus.2.7.0\\lib\\net40-full",
                Strings.WindowsazureServicebus, "2.7.0");
        }

        public static string[] RestoreAzureStorageAssembliy(string installPath, string outputPath) {
            return RestoreNuGetPackage(
                installPath, outputPath, "WindowsAzure.Storage.6.1.0\\lib\\net40", Strings.WindowsazureStorage,
                "6.1.0");
        }

        public static string[] RestoreNewtonsoftJsonAssembliy(string installPath, string outputPath) {
            return RestoreNuGetPackage(
                installPath, outputPath, "Newtonsoft.Json.8.0.3\\lib\\net40", Strings.NewtonsoftJson, "8.0.3");
        }

        public static string[] RestoreDotlessCoreAssembliy(string installPath, string outputPath) {
            return RestoreNuGetPackage(installPath, outputPath, "dotless.1.5.2\\lib", "dotless", "1.5.2");
        }

        public static string[] RestoreSharpZipLibAssembliy(string installPath, string outputPath) {
            return RestoreNuGetPackage(installPath, outputPath, "SharpZipLib.0.86.0\\lib\\20", "SharpZipLib", "0.86.0");
        }

        private static string[] CopyAssemblies(string installPath, string outputPath, string assembliesInPackagePath) {
            List<string> list = new List<string>();
            string path = Path.Combine(installPath, Strings.NugetPackagesInstallationPath, assembliesInPackagePath);
            bool flag = Directory.Exists(path);
            if (flag) {
                list.AddRange(Directory.GetFiles(path));
                bool flag2 = list.Count > 0;
                if (flag2) {
                    foreach (string current in list) {
                        try {
                            File.Copy(current, Path.Combine(outputPath, Path.GetFileName(current)), true);
                        } catch (Exception) {
                            Logger.Instance.AddText(
                                string.Format(
                                    "Error copying {0} file to {1} folder", current,
                                    Path.Combine(outputPath, Path.GetFileName(current))), TextItemStatus.Error);
                        }
                    }
                }
            }
            return list.ToArray();
        }

        private static void InstallPackage(string packageId, string version, string path) {
            ExecuteCommand(
                string.Format(
                    Strings.NugetInstallPackageArgs, packageId, version,
                    Path.Combine(path, Strings.NugetPackagesInstallationPath)), path);
        }

        private static void RemovePackages(string path) {
            try {
                Directory.Delete(Path.Combine(path, Strings.NugetPackagesInstallationPath), true);
            } catch (Exception ex) {
                Logger.Instance.AddText(
                    string.Format(Strings.PackagesNotDeleted, ex.Message), TextItemStatus.Warning);
            }
        }

        private static void ExecuteCommand(string arguments, string dir) {
            Process process = new Process();
            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.Verb = "runas";
            processStartInfo.FileName = _nuget;
            processStartInfo.Arguments = arguments;
            bool flag = !string.IsNullOrEmpty(dir);
            if (flag) {
                processStartInfo.WorkingDirectory = dir;
            }
            process.StartInfo = processStartInfo;
            process.Start();
            process.WaitForExit();
        }

        private static string DownloadNuget(string path) {
            string text = Path.Combine(path, Strings.NugetExe);
            using (WebClient webClient = new WebClient()) {
                webClient.DownloadFile(Strings.NugetDirectLink, text);
            }
            bool flag = File.Exists(text);
            string empty;
            if (flag) {
                _nuget = text;
                empty = _nuget;
            } else {
                empty = string.Empty;
            }
            return empty;
        }
    }
}