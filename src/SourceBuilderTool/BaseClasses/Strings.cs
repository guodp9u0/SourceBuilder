namespace SourceBuilderTool.BaseClasses {
    internal static class Strings {
        public static readonly string NugetPackagesInstallationPath = "NuGetPackages";

        public static readonly string NugetExe = "nuget.exe";

        public static readonly string NugetDirectLink = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe";

        public static readonly string WindowsazureServicebus = "WindowsAzure.ServiceBus";

        public static readonly string MonoCecil = "Mono.Cecil";

        public static readonly string WindowsazureStorage = "WindowsAzure.Storage";

        public static string LockedFilesFoundMessage =
            "There are files that were not deleted. Would you like to try to delete them again?";

        public static string LockedFilesFoundCaption = "Locked files found";

        public static string LockedFilesStillNotDeleted =
            "Certain assemblies were not deleted from GAC. Please reboot your machine and try again";

        public static string FileNotDeleted = "{0} is not deleted.";

        internal static string NoInternetConnection =
                "Machine is not connected to Internet. Mono.Cecil assemblies are required to build DevExpress.XPF.Core.Please place following assebmlies to the\r\n {0} \r\ndirectory and press OK when done:\r\n\t1.Mono.Cecil.dll\r\n\t2.Mono.Cecil.Mdb.dll\r\n\t3.Mono.Cecil.Pdb.dll\r\n\t4.Mono.Cecil.Rocks.dll\r\nTo get the assemblies, execute the following command in the Visual Studio's Package Manager console:\r\nInstall-Package Mono.Cecil -Version 0.9.6.1\r\nThen locate the \\packages\\Mono.Cecil.0.9.6.1\\lib\\net40 folder containing the assemblies."
            ;

        internal static string NoInternetConnectionCaption = "Unable to restore Mono.Cecil assemblies";

        internal static string PackagesNotDeleted = "Error deleting packages. {0}";

        internal static string NugetInstallPackageArgs =
            "install {0} /NonInteractive /Version {1} /OutPutDirectory \"{2}\"";

        public static string NewtonsoftJson = "Newtonsoft.Json";

        public static string DotlessJson = "dotless";
    }
}