namespace SourceBuilderTool.Builder {
    public class PublicKeys {
        private string _publicKeyName;

        private string _publicKeyToken;

        public string PublicKeyName {
            get {
                return _publicKeyName;
            }
        }

        public string PublicKeyToken {
            get {
                return _publicKeyToken;
            }
        }

        public PublicKeys(string publicKeyName, string publicKeyToken) {
            _publicKeyName = publicKeyName;
            _publicKeyToken = publicKeyToken;
        }
    }
}