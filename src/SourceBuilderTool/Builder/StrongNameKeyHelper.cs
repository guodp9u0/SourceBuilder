#region

using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;

#endregion

namespace SourceBuilderTool.Builder {
    public static class StrongNameKeyHelper {
        public static PublicKeys GetPublicKeyToken(string snkPath) {
            bool flag = !File.Exists(snkPath);
            PublicKeys result;
            if (flag) {
                result = null;
            } else {
                FileStream fileStream = File.Open(snkPath, FileMode.Open, FileAccess.Read);
                StrongNameKeyPair strongNameKeyPair = new StrongNameKeyPair(fileStream);
                string publicKeyTokenName = GetPublicKeyTokenName(strongNameKeyPair);
                PublicKeys publicKeys = new PublicKeys(publicKeyTokenName, GetPublicKeyToken(strongNameKeyPair));
                Logger.Instance.AddText(
                    string.Format("Public key token: {0}", publicKeyTokenName), TextItemStatus.Info);
                fileStream.Close();
                result = publicKeys;
            }
            return result;
        }

        private static string GetPublicKeyTokenName(StrongNameKeyPair k) {
            return ByteArrayToString(GetPublicKeyToken(k.PublicKey));
        }

        private static string GetPublicKeyToken(StrongNameKeyPair snkPair) {
            return ByteArrayToString(snkPair.PublicKey);
        }

        private static string ByteArrayToString(byte[] ba) {
            string text = BitConverter.ToString(ba);
            return text.Replace("-", "").ToLower();
        }

        private static byte[] GetPublicKeyToken(byte[] publicKey) {
            byte[] result;
            using (SHA1CryptoServiceProvider sHa1CryptoServiceProvider = new SHA1CryptoServiceProvider()) {
                byte[] array = sHa1CryptoServiceProvider.ComputeHash(publicKey);
                byte[] array2 = new byte[8];
                for (int i = 0; i < 8; i++) {
                    array2[i] = array[array.Length - i - 1];
                }
                result = array2;
            }
            return result;
        }
    }
}