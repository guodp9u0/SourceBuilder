#region

using System;
using System.Diagnostics;
using System.IO;
using System.Text;

#endregion

namespace SourceBuilderTool {
    public static class CommandPromptOperationHelper {
        private static Encoding GetOemEncoding() {
            Encoding result;
            try {
                result = Encoding.GetEncoding(UnsafeNativeMethods.GetCpInfoExWSafe().CodePage);
            } catch (InvalidOperationException ex) {
                Logger.Instance.AddText("\r\n" + ex.Message, TextItemStatus.Error);
                result = Encoding.Default;
            }
            return result;
        }

        public static string ExecuteCommand(string command, string folder) {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo {
                FileName = "cmd",
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                Verb = "runas",
                WorkingDirectory = folder,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                StandardOutputEncoding = GetOemEncoding()
            };
            process.Start();
            StreamReader standardOutput = process.StandardOutput;
            StreamWriter standardInput = process.StandardInput;
            standardInput.WriteLine(command);
            standardInput.WriteLine("exit");
            string result = standardOutput.ReadToEnd();
            standardInput.Close();
            standardOutput.Close();
            return result;
        }
    }
}