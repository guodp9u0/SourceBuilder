#region

using System.Collections.Generic;
using DevExpress.Utils.Implementation;

#endregion

namespace DevExpress.Utils {
    public static class Algorithms {
        public static IList<T> TopologicalSort<T>(IList<T> sourceObjects, IComparer<T> comparer) {
            TopologicalSorter<T> topologicalSorter = new TopologicalSorter<T>();
            return topologicalSorter.Sort(sourceObjects, comparer);
        }
    }
}