#region

using System.Collections.Generic;

#endregion

namespace DevExpress.Utils.Implementation {
    public class TopologicalSorter<T> {
        public class Node {
            private readonly int _refCount;

            private readonly Node _next;

            public int RefCount {
                get {
                    return _refCount;
                }
            }

            public Node Next {
                get {
                    return _next;
                }
            }

            public Node(int refCount, Node next) {
                this._refCount = refCount;
                this._next = next;
            }
        }

        private int[] _qLink;

        private Node[] _nodes;

        private IList<T> _sourceObjects;

        private IComparer<T> _comparer;

        protected internal Node[] Nodes {
            get {
                return _nodes;
            }
        }

        protected internal int[] QLink {
            get {
                return _qLink;
            }
        }

        protected IComparer<T> Comparer {
            get {
                return _comparer;
            }
        }

        protected internal IList<T> SourceObjects {
            get {
                return _sourceObjects;
            }
        }

        protected IComparer<T> GetComparer() {
            IComparer<T> arg180;
            if (Comparer == null) {
                IComparer<T> @default = Comparer<T>.Default;
                arg180 = @default;
            } else {
                arg180 = Comparer;
            }
            return arg180;
        }

        protected bool IsDependOn(T x, T y) {
            return GetComparer().Compare(x, y) > 0;
        }

        public IList<T> Sort(IList<T> sourceObjects, IComparer<T> comparer) {
            this._comparer = comparer;
            return Sort(sourceObjects);
        }

        public IList<T> Sort(IList<T> sourceObjects) {
            this._sourceObjects = sourceObjects;
            int count = sourceObjects.Count;
            bool flag = count < 2;
            IList<T> result;
            if (flag) {
                result = sourceObjects;
            } else {
                Initialize(count);
                CalculateRelations(sourceObjects);
                int r = FindNonRelatedNodeIndex();
                IList<T> list = ProcessNodes(r);
                result = ((list.Count > 0) ? list : sourceObjects);
            }
            return result;
        }

        protected internal void Initialize(int n) {
            int num = n + 1;
            _qLink = new int[num];
            _nodes = new Node[num];
        }

        protected internal void CalculateRelations(IList<T> sourceObjects) {
            int count = sourceObjects.Count;
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count; j++) {
                    bool flag = !IsDependOn(sourceObjects[i], sourceObjects[j]);
                    if (!flag) {
                        int num = j + 1;
                        int num2 = i + 1;
                        QLink[num2]++;
                        Nodes[num] = new Node(num2, Nodes[num]);
                    }
                }
            }
        }

        protected internal int FindNonRelatedNodeIndex() {
            int num = 0;
            int count = SourceObjects.Count;
            for (int i = 0; i <= count; i++) {
                bool flag = QLink[i] == 0;
                if (flag) {
                    QLink[num] = i;
                    num = i;
                }
            }
            return num;
        }

        protected virtual IList<T> ProcessNodes(int r) {
            int count = _sourceObjects.Count;
            int num = count;
            int i = QLink[0];
            List<T> list = new List<T>(count);
            while (i > 0) {
                list.Add(_sourceObjects[i - 1]);
                num--;
                for (Node node = Nodes[i]; node != null; node = RemoveRelation(node, ref r)) {
                }
                i = QLink[i];
            }
            return list;
        }

        private Node RemoveRelation(Node node, ref int r) {
            int refCount = node.RefCount;
            QLink[refCount]--;
            bool flag = QLink[refCount] == 0;
            if (flag) {
                QLink[r] = refCount;
                r = refCount;
            }
            return node.Next;
        }
    }
}