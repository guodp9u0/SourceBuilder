namespace SourceBuilderTool {
    public class Dxperience {
        public Dxperience Value {
            get {
                return this;
            }
        }

        public string Version {
            get;
            set;
        }

        public string SourceFolder {
            get;
            set;
        }

        public string RootFolder {
            get;
            set;
        }

        public bool SourcesAvailable {
            get {
                return string.IsNullOrEmpty(SourceFolder);
            }
        }
    }
}