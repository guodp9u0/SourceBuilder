#region

using System;
using System.IO;
using System.Linq;

#endregion

namespace SourceBuilderTool {
    public static class Exclusions {
        private static readonly string UwpProjectGuid =
                "<ProjectTypeGuids>{A5A43C5B-DE2A-4C0C-9213-0A381AF9435A};{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}</ProjectTypeGuids>"
            ;

        private static readonly string DxSampleEmbeddedProjectName = "DxSample";

        private static string[] _isAssemblyNotToDeleteFromGacArray = {
            "Build.XamlResourceProcessing",
            "DevExpress.ExpressApp.DesignCore",
            "DevExpress.EasyTest"
        };

        private static string[] _isAssemblyToRemoveFromBuildListArray = {
            "DevExpress.Mvvm.UI.ApplicationJumpTaskLauncher",
            "DevExpress.Xpf.DemoBase",
            "DevExpress.ExpressApp.ModelEditor",
            "DBUpdater",
            "DevExpress.ExpressApp.EasyTest.WebAdapter",
            "DevExpress.ExpressApp.EasyTest.WinAdapter",
            "DevExpress.Win.FunctionalTests",
            "ExpressApp.Updater"
        };

        private static string[] _isAssemblyToRemoveFromReferencesListArray = {
            "DevExpress.EasyTest",
            "DevExpress.ExpressApp.DesignCore",
            "DevExpress.WinRTPresenter.BackgroundTasks"
        };

        public static bool IsAssemblyNotToDeleteFromGac(string assemblyName) {
            return StringContainsAnElementFromArray(_isAssemblyNotToDeleteFromGacArray, assemblyName);
        }

        public static bool IsAssemblyToRemoveFromBuildList(string assemblyName) {
            return StringContainsAnElementFromArray(_isAssemblyToRemoveFromBuildListArray, assemblyName);
        }

        public static bool IsAssemblyToRemoveFromReferencesList(string assemblyName) {
            return StringContainsAnElementFromArray(_isAssemblyToRemoveFromReferencesListArray, assemblyName);
        }

        public static bool IsEmbeddedTemplateProject(Project project) {
            return project.AssemblyName.Equals(DxSampleEmbeddedProjectName);
        }

        public static bool IsUwpProject(Project project) {
            bool result;
            try {
                using (StreamReader streamReader = new StreamReader(project.FullPath)) {
                    string text = streamReader.ReadToEnd();
                    bool flag = text.Contains(UwpProjectGuid) || IsUwpProject(project, text)
                                || (project.AssemblyName.StartsWith("DevExpress.Core")
                                    && project.AssemblyName.EndsWith("Design"));
                    if (flag) {
                        result = true;
                        return result;
                    }
                }
            } catch (Exception var4_6E) {
            }
            result = false;
            return result;
        }

        private static bool IsUwpProject(Project project, string line) {
            return line.Contains("<OutputPath>..\\..\\Bin\\UAP\\") || line.Contains("<OutputPath>..\\..\\Bin\\UWP\\")
                   || project.AssemblyName.StartsWith("DevExpress.UI.XAML");
        }

        private static bool StringContainsAnElementFromArray(string[] array, string str) {
            return array.Any(s => str.Contains(s));
        }
    }
}