#region

using System.Collections.Generic;
using System.IO;

#endregion

namespace SourceBuilderTool {
    public static class FileOperations {
        public static List<string> LockedFiles = new List<string>();

        public static List<string> LockedDirectories = new List<string>();

        public static void SaveStringToFile(string text, string path) {
            using (StreamWriter streamWriter = File.CreateText(path)) {
                streamWriter.Write(text);
                streamWriter.Flush();
            }
        }

        public static void DeleteAllAssembliesInTheFolder(string folder) {
            string[] files = Directory.GetFiles(folder, "*.dll", SearchOption.AllDirectories);
            string[] array = files;
            for (int i = 0; i < array.Length; i++) {
                string text = array[i];
                try {
                    File.Delete(text);
                    Logger.Instance.AddText(text + " was deleted", TextItemStatus.Success);
                } catch {
                    LockedFiles.Add(text);
                    Logger.Instance.AddText(
                        "Cannot delete " + text + " - possibly it is locked by another process", TextItemStatus.Error);
                }
            }
        }

        public static void DeleteFolder(string folder) {
            try {
                Directory.Delete(folder, true);
                Logger.Instance.AddText(folder + " was deleted", TextItemStatus.Success);
            } catch {
                LockedDirectories.Add(folder);
                Logger.Instance.AddText(
                    "Cannot delete " + folder + " - possibly it is locked by another process", TextItemStatus.Error);
            }
        }
    }
}