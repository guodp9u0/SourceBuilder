#region

using System;
using System.Runtime.InteropServices;

#endregion

namespace SourceBuilderTool {
    public static class GacUtil {
        [Guid("e707dcde-d1cd-11d2-bab9-00c04f8eceae"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport]
        private interface IAssemblyCache {
            int Dummy1();

            [PreserveSig]
            IntPtr QueryAssemblyInfo(int flags, [MarshalAs(UnmanagedType.LPWStr)] string assemblyName,
                ref AssemblyInfo assemblyInfo);

            int Dummy2();

            int Dummy3();

            int Dummy4();
        }

        private struct AssemblyInfo {
            public int CbAssemblyInfo;

            public int AssemblyFlags;

            public long AssemblySizeInKb;

            [MarshalAs(UnmanagedType.LPWStr)]
            public string CurrentAssemblyPath;

            public int CchBuf;
        }

        [DllImport("fusion.dll")]
        private static extern IntPtr CreateAssemblyCache(out IAssemblyCache ppAsmCache, int reserved);

        public static bool IsAssemblyInGac(string assemblyName) {
            AssemblyInfo assemblyInfo = new AssemblyInfo {
                CchBuf = 512
            };
            assemblyInfo.CurrentAssemblyPath = new string('\0', assemblyInfo.CchBuf);
            IAssemblyCache assemblyCache;
            IntPtr value = CreateAssemblyCache(out assemblyCache, 0);
            bool flag = value == IntPtr.Zero;
            bool result;
            if (flag) {
                value = assemblyCache.QueryAssemblyInfo(1, assemblyName, ref assemblyInfo);
                bool flag2 = value != IntPtr.Zero;
                result = !flag2;
            } else {
                Marshal.ThrowExceptionForHR(value.ToInt32());
                result = false;
            }
            return result;
        }
    }
}