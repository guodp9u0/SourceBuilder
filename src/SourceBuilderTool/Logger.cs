#region

using System;
using System.ComponentModel;
using System.Linq;

#endregion

namespace SourceBuilderTool {
    public class Logger {
        private static Logger _instance;

        private BindingList<TextItem> _text;

        private int _progressValue;

        private int _maxProgressValue;

        public static Logger Instance {
            get {
                bool flag = _instance == null;
                if (flag) {
                    _instance = new Logger();
                }
                return _instance;
            }
        }

        public string Plaintext {
            get {
                string text = "";
                foreach (TextItem current in Text.ToList()) {
                    text = text + current.Text + Environment.NewLine + Environment.NewLine;
                }
                return text;
            }
        }

        public BindingList<TextItem> Text {
            get {
                return _text;
            }
        }

        public int SuccessfullyBuildProjectCount {
            get;
            set;
        }

        public int ProgressValue {
            get {
                return _progressValue;
            }
            set {
                _progressValue = value;
                bool flag = !ApplicationSettings.Instance.CommandPromptMode;
                if (flag) {
                    MainForm.Instance.SetProgressValue(_progressValue);
                }
            }
        }

        public int MaxProgressValue {
            get {
                return _maxProgressValue;
            }
            set {
                _maxProgressValue = value;
                bool flag = !ApplicationSettings.Instance.CommandPromptMode;
                if (flag) {
                    MainForm.Instance.SetMaxProgressValue(_maxProgressValue);
                }
            }
        }

        public Logger() {
            _text = new BindingList<TextItem>();
        }

        public void AddText(string text, TextItemStatus status) {
            bool flag = !string.IsNullOrEmpty(text);
            if (flag) {
                this._text.Add(
                    new TextItem {
                        Index = this._text.Count + 1,
                        Text = string.Format("{0} {1}", DateTime.Now, text),
                        Status = status
                    });
                bool flag2 = status == TextItemStatus.Error;
                if (flag2) {
                    SaveToFile("build.log");
                }
            }
        }

        internal static void SaveToFile(string path) {
            FileOperations.SaveStringToFile(Instance.Plaintext, path);
        }

        internal static void SaveErrorsToFile(string path) {
            string str = "";
            foreach (TextItem current in Instance.Text) {
                bool flag = current.Status == TextItemStatus.Error;
                if (flag) {
                    string str2 = current.Text.Split(
                        new[] {
                            Environment.NewLine
                        }, 2, StringSplitOptions.None)[0];
                    str = str + str2 + Environment.NewLine;
                }
            }
            FileOperations.SaveStringToFile(str, path);
        }
    }
}