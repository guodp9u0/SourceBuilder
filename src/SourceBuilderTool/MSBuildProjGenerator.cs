#region

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

#endregion

namespace SourceBuilderTool {
    internal class MsBuildProjGenerator {
        public static void ProcessProjects(IList<Project> loadedProjects) {
            XmlDocument xmlDocument = new XmlDocument();
            using (Stream manifestResourceStream = Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("SourceBuilderTool.Resources.build.bat")) {
                using (Stream stream = File.Create(
                    ApplicationSettings.Instance.DxVersionToBuild.SourceFolder + "build.bat")) {
                    CopyStream(manifestResourceStream, stream);
                }
            }
            Stream manifestResourceStream2 = Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("SourceBuilderTool.Resources.MyProduct.build.proj");
            manifestResourceStream2.Seek(0L, SeekOrigin.Begin);
            xmlDocument.Load(manifestResourceStream2);
            manifestResourceStream2.Close();
            XmlNode xmlNode = xmlDocument.FirstChild.ChildNodes[1];
            for (int i = loadedProjects.Count - 1; i > -1; i--) {
                XmlElement xmlElement = xmlDocument.CreateElement("Projects", "");
                xmlElement.SetAttribute("Include", loadedProjects[i].FullPath);
                xmlNode.AppendChild(xmlElement);
            }
            xmlDocument.Save("proj.txt");
            string text = File.ReadAllText("proj.txt");
            File.Delete("proj.txt");
            text = text.Replace("xmlns=\"\"", "");
            File.WriteAllText(
                ApplicationSettings.Instance.DxVersionToBuild.SourceFolder + "MyProduct.build.proj", text);
            Logger.Instance.AddText(
                "MS Build file generated at " + ApplicationSettings.Instance.DxVersionToBuild.SourceFolder,
                TextItemStatus.Info);
        }

        public static void CopyStream(Stream input, Stream output) {
            byte[] array = new byte[input.Length];
            int count;
            while ((count = input.Read(array, 0, array.Length)) > 0) {
                output.Write(array, 0, count);
            }
        }
    }
}