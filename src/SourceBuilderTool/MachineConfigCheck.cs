#region

using System;
using System.IO;
using System.Net;
using Microsoft.Build.Utilities;
using Microsoft.Win32;

#endregion

namespace SourceBuilderTool {
    public static class MachineConfigCheck {
        public static string CheckMachineSettings(SystemSettings settings) {
            string text = "";
            bool flag = !Check45FrameworkInstalled();
            if (flag) {
                text =
                    text
                    + ".NET Framework 4.5 is not detected - XPF controls could not be build - Please install .Net Framework 4.5"
                    + Environment.NewLine;
            }
            bool flag2 = !CheckRequiredVs2010Assemblies(settings.ProgramFilesFolder);
            if (flag2) {
                text =
                    text
                    + "Required VS2010 SP1 IDE assemblies are not detected - XPF controls could not be build - Please install VS2010 or VS2010 Express (C# or VB)"
                    + Environment.NewLine;
            }
            bool flag3 = !CanBuildSilvelight();
            if (flag3) {
                text =
                    text
                    + "Silverlight 5 is not detected - Silverlight controls could not be build - Please install Silverlight 5.0 SDK"
                    + Environment.NewLine;
            }
            bool flag4 = !IsSharePointDetected();
            if (flag4) {
                text =
                    text
                    + "Sharepoint assemblies were not detected on machine- DevExpress.SharePoint assembly could not be build"
                    + Environment.NewLine;
            }
            bool flag5 = !IsMvcDetected();
            if (flag5) {
                text = text + "MVC assemblies were not detected on machine- DevExpress.Web.Mvc* could not be build"
                       + Environment.NewLine;
            }
            return text;
        }

        public static string CheckAllBuildToolsAvailable(SystemSettings settings) {
            string text = "";
            bool flag = !File.Exists(settings.BuildSettings.GacUtilPath);
            if (flag) {
                text = "GacUtil tool is not found at :" + settings.BuildSettings.GacUtilPath + Environment.NewLine;
            }
            bool flag2 = !File.Exists(settings.BuildSettings.MsBuildPath);
            if (flag2) {
                text = text + "MSBuild tool is not found at :" + settings.BuildSettings.MsBuildPath
                       + Environment.NewLine;
            }
            bool flag3 = !File.Exists(settings.BuildSettings.SnPath);
            if (flag3) {
                text = text + "Sn tool is not found at :" + settings.BuildSettings.SnPath;
            }
            return text;
        }

        private static bool CheckRequiredVs2010Assemblies(string programFilesFolder) {
            string path = programFilesFolder
                          + "\\Microsoft Visual Studio 10.0\\Common7\\IDE\\PublicAssemblies\\Microsoft.Windows.Design.Extensibility.dll";
            string path2 = programFilesFolder
                           + "\\Microsoft Visual Studio 10.0\\Common7\\IDE\\PublicAssemblies\\Microsoft.Windows.Design.Interaction.dll";
            return File.Exists(path) && File.Exists(path2);
        }

        private static bool IsSharePointDetected() {
            return IsAssemblyExist(
                "Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c, processorArchitecture=MSIL");
        }

        private static bool IsMvcDetected() {
            return true;
        }

        public static bool CheckSystemErrorInfo(SystemSettings settings) {
            bool result = false;
            bool flag = !CheckForInternetConnection();
            if (flag) {
                Logger.Instance.AddText("Machine does not have Internet connection.", TextItemStatus.Error);
            }
            bool flag2 = settings.DxVersionsInstalled.Count == 0;
            if (flag2) {
                Logger.Instance.AddText(
                    "Error reading registry, please make sure that you are running this tool under admin rights",
                    TextItemStatus.Error);
                ApplicationWorkflow.Instance.IsBuildPossible = false;
                result = true;
            }
            bool flag3 = ApplicationSettings.Instance.DxVersionToBuild == null;
            if (flag3) {
                Logger.Instance.AddText("Incorrect version of the suite is selected", TextItemStatus.Error);
                ApplicationWorkflow.Instance.IsBuildPossible = false;
                result = true;
            }
            string text = CheckAllBuildToolsAvailable(SystemSettings.Instance);
            bool flag4 = !string.IsNullOrEmpty(text);
            if (flag4) {
                Logger.Instance.AddText(text, TextItemStatus.Error);
                ApplicationWorkflow.Instance.IsBuildPossible = false;
                result = true;
            }
            text = CheckMachineSettings(SystemSettings.Instance);
            bool flag5 = !string.IsNullOrEmpty(text);
            if (flag5) {
                string[] array = text.Split(
                    new[] {
                        Environment.NewLine
                    }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < array.Length; i++) {
                    string text2 = array[i];
                    Logger.Instance.AddText(text2, TextItemStatus.Warning);
                }
                result = true;
            }
            return result;
        }

        public static bool IsAssemblyExist(string s) {
            return GacUtil.IsAssemblyInGac(s);
        }

        private static bool Check45FrameworkInstalled() {
            return ToolLocationHelper.GetSupportedTargetFrameworks().Contains(".NETFramework,Version=v4.5");
        }

        private static bool CanBuildSilvelight() {
            object value = Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Silverlight", "Version", null);
            bool flag = value == null;
            return !flag && Convert.ToInt32(value.ToString()[0]) >= 5;
        }

        public static bool CheckForInternetConnection() {
            bool result;
            try {
                using (WebClient webClient = new WebClient()) {
                    using (webClient.OpenRead("http://www.google.com")) {
                        result = true;
                    }
                }
            } catch {
                result = false;
            }
            return result;
        }
    }
}