#region

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

#endregion

namespace SourceBuilderTool {
    public class MainForm : Form {
        private DataGridViewCellStyle _styleSuccess = new DataGridViewCellStyle();

        private DataGridViewCellStyle _styleWarning = new DataGridViewCellStyle();

        private DataGridViewCellStyle _styleInfo = new DataGridViewCellStyle();

        private DataGridViewCellStyle _styleError = new DataGridViewCellStyle();

        private static MainForm _instance;

        private IContainer components = null;

        private ComboBox _comboBox1;

        private Label _label1;

        private ComboBox _comboBox3;

        private Label _label3;

        private DataGridView _dataGridView1;

        private DataGridViewTextBoxColumn _textColumn;

        private Label _label2;

        private ProgressBar _progressBar1;

        private Button _button2;

        private Button _btnClearGac;

        private Button _btnRegisterAssemblies;

        private CheckBox _chbAutoscroll;

        public static MainForm Instance {
            get {
                bool flag = _instance == null;
                if (flag) {
                    _instance = new MainForm();
                }
                return _instance;
            }
        }

        public MainForm() {
            InitializeComponent();
            _styleSuccess.ForeColor = Color.Black;
            _styleSuccess.BackColor = Color.LightGreen;
            _styleInfo.BackColor = Color.LightBlue;
            _styleInfo.ForeColor = Color.Black;
            _styleError.BackColor = Color.PaleVioletRed;
            _styleError.ForeColor = Color.Black;
            _styleWarning.BackColor = Color.Orange;
            _styleWarning.ForeColor = Color.Black;
            _comboBox3.Items.AddRange(
                new[] {
                    "Release",
                    "Debug"
                });
            _comboBox3.DataBindings.Add("SelectedItem", ApplicationSettings.Instance, "BuildConfiguration");
            _dataGridView1.AutoGenerateColumns = false;
            _dataGridView1.DataSource = Logger.Instance.Text;
            _dataGridView1.RowsAdded += DataGridView1_RowsAdded;
            _dataGridView1.MouseWheel += DataGridView1_MouseWheel;
            _dataGridView1.CellFormatting += DataGridView1_CellFormatting;
            _dataGridView1.CellDoubleClick += DataGridView1_CellDoubleClick;
            _comboBox1.DataSource = SystemSettings.Instance.DxVersionsInstalled;
            _comboBox1.DisplayMember = "Version";
            _comboBox1.DataBindings.Add("SelectedItem", ApplicationSettings.Instance, "DxVersionToBuild");
            Text = "DevExpress Source Code Builder Tool v" + ((AssemblyFileVersionAttribute) Assembly
                       .GetExecutingAssembly()
                       .GetCustomAttributes(typeof (AssemblyFileVersionAttribute), true)[0]).Version;
            _button2.Enabled = ApplicationWorkflow.Instance.IsBuildPossible;
            bool adminMode = ApplicationSettings.Instance.AdminMode;
            if (adminMode) {
                _btnClearGac.Visible = true;
                _btnRegisterAssemblies.Visible = true;
            }
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            MessageBox.Show(_dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
        }

        private void DataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e) {
            bool flag = e.Value != null && e.ColumnIndex == 0;
            if (flag) {
                e.Value = e.Value.ToString()
                    .Split(
                        new[] {
                            Environment.NewLine
                        }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
        }

        private void DataGridView1_MouseWheel(object sender, MouseEventArgs e) {
            bool flag = e.Delta > 0;
            if (flag) {
                _chbAutoscroll.Checked = false;
            }
        }

        private void DataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) {
            bool @checked = _chbAutoscroll.Checked;
            if (@checked) {
                _dataGridView1.FirstDisplayedScrollingRowIndex = _dataGridView1.RowCount - 1;
            }
        }

        public void UpdateLog() {
            _dataGridView1.Refresh();
            _dataGridView1.FirstDisplayedScrollingRowIndex = _dataGridView1.RowCount - 1;
        }

        internal void SetMaxProgressValue(int maxProgressValue) {
            _progressBar1.Maximum = maxProgressValue;
            Application.DoEvents();
        }

        internal void SetProgressValue(int progressValue) {
            _progressBar1.Value = ((_progressBar1.Maximum < progressValue) ? _progressBar1.Maximum : progressValue);
            bool flag = progressValue == 0;
            if (flag) {
                _progressBar1.Visible = true;
            }
            bool flag2 = progressValue == _progressBar1.Maximum;
            if (flag2) {
                _progressBar1.Visible = false;
            }
            Application.DoEvents();
        }

        private void button2_Click(object sender, EventArgs e) {
            _button2.Enabled = false;
            _comboBox1.Enabled = false;
            _comboBox3.Enabled = false;
            _chbAutoscroll.Enabled = true;
            _comboBox3.BackColor = Color.White;
            _comboBox1.BackColor = Color.White;
            ApplicationWorkflow.Instance.StartBuildingProcess();
        }

        public void EnableEditors() {
            _button2.Enabled = true;
            _comboBox1.Enabled = true;
            _comboBox3.Enabled = true;
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            foreach (DataGridViewRow dataGridViewRow in _dataGridView1.Rows) {
                bool flag = dataGridViewRow.DataBoundItem != null;
                if (flag) {
                    switch (((TextItem) dataGridViewRow.DataBoundItem).Status) {
                        case TextItemStatus.Info:
                            dataGridViewRow.Cells[0].Style = _styleInfo;
                            break;
                        case TextItemStatus.Warning:
                            dataGridViewRow.Cells[0].Style = _styleWarning;
                            break;
                        case TextItemStatus.Error:
                            dataGridViewRow.Cells[0].Style = _styleError;
                            break;
                        case TextItemStatus.Success:
                            dataGridViewRow.Cells[0].Style = _styleSuccess;
                            break;
                    }
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e) {
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            bool buildInProgress = ApplicationWorkflow.Instance.BuildInProgress;
            if (buildInProgress) {
                e.Cancel = true;
                DialogResult dialogResult = MessageBox.Show(
                    "Closing Source Code Builder Tool during the rebuilding process may cause instable work of Devexpress components. Close anyway?",
                    "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.RightAlign);
                bool flag = dialogResult == DialogResult.Yes;
                if (flag) {
                    e.Cancel = false;
                }
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e) {
            _dataGridView1.ClearSelection();
        }

        private void btnClearGAC_Click(object sender, EventArgs e) {
            DialogResult dialogResult = MessageBox.Show(
                "Remove " + ApplicationSettings.Instance.DxVersionToBuild.Version + " assemblies from GAC?", "Warning!",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                MessageBoxOptions.RightAlign);
            bool flag = DialogResult.OK == dialogResult;
            if (flag) {
                ApplicationWorkflow.Instance.ClearGac();
            }
        }

        private void btnRegisterAssemblies_Click(object sender, EventArgs e) {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.SelectedPath = ApplicationSettings.Instance.DxVersionToBuild.RootFolder;
            folderBrowserDialog.ShowNewFolderButton = false;
            bool flag = folderBrowserDialog.ShowDialog() == DialogResult.OK;
            if (flag) {
                string[] files = Directory.GetFiles(
                    folderBrowserDialog.SelectedPath, "*.dll", SearchOption.AllDirectories);
                string[] array = files;
                for (int i = 0; i < array.Length; i++) {
                    string assemblyFullPath = array[i];
                    ProjectsBuilder.RegisterInGac(assemblyFullPath);
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e) {
            Application.Exit();
        }

        protected override void Dispose(bool disposing) {
            bool flag = disposing && components != null;
            if (flag) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            _comboBox3 = new ComboBox();
            _label3 = new Label();
            _button2 = new Button();
            _comboBox1 = new ComboBox();
            _label1 = new Label();
            _dataGridView1 = new DataGridView();
            _textColumn = new DataGridViewTextBoxColumn();
            _label2 = new Label();
            _progressBar1 = new ProgressBar();
            _btnClearGac = new Button();
            _btnRegisterAssemblies = new Button();
            _chbAutoscroll = new CheckBox();
            ((ISupportInitialize) _dataGridView1).BeginInit();
            SuspendLayout();
            _comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
            _comboBox3.Font = new Font("Segoe UI", 11.25f);
            _comboBox3.FormattingEnabled = true;
            _comboBox3.Location = new Point(16, 100);
            _comboBox3.Name = "_comboBox3";
            _comboBox3.Size = new Size(208, 28);
            _comboBox3.TabIndex = 11;
            _label3.AutoSize = true;
            _label3.Font = new Font("Segoe UI", 11.25f);
            _label3.Location = new Point(12, 77);
            _label3.Name = "_label3";
            _label3.Size = new Size(100, 20);
            _label3.TabIndex = 12;
            _label3.Text = "Configuration";
            _button2.FlatStyle = FlatStyle.System;
            _button2.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _button2.Location = new Point(16, 174);
            _button2.Name = "_button2";
            _button2.Size = new Size(208, 42);
            _button2.TabIndex = 9;
            _button2.Text = "Build";
            _button2.UseVisualStyleBackColor = true;
            _button2.Click += button2_Click;
            _comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            _comboBox1.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _comboBox1.FormattingEnabled = true;
            _comboBox1.Location = new Point(16, 32);
            _comboBox1.Name = "_comboBox1";
            _comboBox1.Size = new Size(208, 28);
            _comboBox1.TabIndex = 3;
            _label1.AutoSize = true;
            _label1.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _label1.Location = new Point(12, 9);
            _label1.Name = "_label1";
            _label1.Size = new Size(57, 20);
            _label1.TabIndex = 4;
            _label1.Text = "Version";
            _dataGridView1.AllowUserToAddRows = false;
            _dataGridView1.AllowUserToDeleteRows = false;
            _dataGridView1.AllowUserToResizeColumns = false;
            _dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle.BackColor = SystemColors.Control;
            dataGridViewCellStyle.Font = new Font(
                "Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
            _dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
            _dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _dataGridView1.ColumnHeadersVisible = false;
            _dataGridView1.Columns.AddRange(_textColumn);
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = SystemColors.Window;
            dataGridViewCellStyle2.Font = new Font(
                "Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
            _dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            _dataGridView1.Location = new Point(242, 32);
            _dataGridView1.MultiSelect = false;
            _dataGridView1.Name = "_dataGridView1";
            _dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = SystemColors.Control;
            dataGridViewCellStyle3.Font = new Font(
                "Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
            _dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            _dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            _dataGridView1.ShowEditingIcon = false;
            _dataGridView1.Size = new Size(649, 411);
            _dataGridView1.TabIndex = 0;
            _dataGridView1.DataBindingComplete += dataGridView1_DataBindingComplete;
            _dataGridView1.SelectionChanged += dataGridView1_SelectionChanged;
            _textColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            _textColumn.DataPropertyName = "Text";
            dataGridViewCellStyle5.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            dataGridViewCellStyle5.WrapMode = DataGridViewTriState.True;
            _textColumn.DefaultCellStyle = dataGridViewCellStyle5;
            _textColumn.HeaderText = "Text";
            _textColumn.Name = "_textColumn";
            _textColumn.ReadOnly = true;
            _label2.AutoSize = true;
            _label2.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _label2.Location = new Point(238, 9);
            _label2.Name = "_label2";
            _label2.Size = new Size(49, 20);
            _label2.TabIndex = 15;
            _label2.Text = "Status";
            _progressBar1.Location = new Point(16, 222);
            _progressBar1.Name = "_progressBar1";
            _progressBar1.Size = new Size(208, 20);
            _progressBar1.Style = ProgressBarStyle.Continuous;
            _progressBar1.TabIndex = 14;
            _progressBar1.Visible = false;
            _btnClearGac.FlatStyle = FlatStyle.System;
            _btnClearGac.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _btnClearGac.Location = new Point(16, 352);
            _btnClearGac.Name = "_btnClearGac";
            _btnClearGac.Size = new Size(208, 42);
            _btnClearGac.TabIndex = 16;
            _btnClearGac.Text = "Clear GAC";
            _btnClearGac.UseVisualStyleBackColor = true;
            _btnClearGac.Visible = false;
            _btnClearGac.Click += btnClearGAC_Click;
            _btnRegisterAssemblies.FlatStyle = FlatStyle.System;
            _btnRegisterAssemblies.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            _btnRegisterAssemblies.Location = new Point(16, 401);
            _btnRegisterAssemblies.Name = "_btnRegisterAssemblies";
            _btnRegisterAssemblies.Size = new Size(208, 42);
            _btnRegisterAssemblies.TabIndex = 17;
            _btnRegisterAssemblies.Text = "Register Assemblies in GAC";
            _btnRegisterAssemblies.UseVisualStyleBackColor = true;
            _btnRegisterAssemblies.Visible = false;
            _btnRegisterAssemblies.Click += btnRegisterAssemblies_Click;
            _chbAutoscroll.AutoSize = true;
            _chbAutoscroll.Checked = true;
            _chbAutoscroll.CheckState = CheckState.Checked;
            _chbAutoscroll.Location = new Point(16, 249);
            _chbAutoscroll.Name = "_chbAutoscroll";
            _chbAutoscroll.Size = new Size(108, 17);
            _chbAutoscroll.TabIndex = 18;
            _chbAutoscroll.Text = "Auto scroll output";
            _chbAutoscroll.UseVisualStyleBackColor = true;
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(903, 467);
            Controls.Add(_chbAutoscroll);
            Controls.Add(_btnRegisterAssemblies);
            Controls.Add(_btnClearGac);
            Controls.Add(_comboBox3);
            Controls.Add(_comboBox1);
            Controls.Add(_label2);
            Controls.Add(_button2);
            Controls.Add(_label3);
            Controls.Add(_dataGridView1);
            Controls.Add(_progressBar1);
            Controls.Add(_label1);
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            Name = "MainForm";
            FormClosing += MainForm_FormClosing;
            FormClosed += MainForm_FormClosed;
            Load += MainForm_Load;
            ((ISupportInitialize) _dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }
    }
}