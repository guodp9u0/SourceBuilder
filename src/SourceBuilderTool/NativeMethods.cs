#region

using System.Runtime.InteropServices;

#endregion

namespace SourceBuilderTool {
    public static class NativeMethods {
        internal const int AttachParentProcess = -1;

        [DllImport("kernel32.dll")]
        internal static extern bool AttachConsole(int dwProcessId);
    }
}