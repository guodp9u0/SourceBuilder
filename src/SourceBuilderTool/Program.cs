#region

using System;
using System.Windows.Forms;

#endregion

namespace SourceBuilderTool {
    internal static class Program {
        [STAThread]
        public static void Main(string[] args) {
            ApplicationWorkflow.Instance.IsBuildPossible = true;
            NativeMethods.AttachConsole(-1);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try {
                ApplicationWorkflow.ProcessInputParameter(args);
                ApplicationWorkflow.Instance.Run();
            } catch (Exception ex) {
                Logger.Instance.AddText(ex.Message, TextItemStatus.Error);
            }
        }
    }
}