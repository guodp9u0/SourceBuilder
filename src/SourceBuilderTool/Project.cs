#region

using System.Collections.Generic;

#endregion

namespace SourceBuilderTool {
    public class Project {
        public string AssemblyName {
            get;
            set;
        }

        public string OutputPath {
            get;
            set;
        }

        public string TargetFrameworkVersion {
            get;
            set;
        }

        public string TargetFrameworkIdentifier {
            get;
            set;
        }

        public List<string> ProjectReferences {
            get;
            set;
        }

        public List<Project> DxProjectReferences {
            get;
            set;
        }

        public List<Project> DxChildProjectReferences {
            get;
            set;
        }

        public ProjectCategory Category {
            get;
            set;
        }

        public bool IsSomeReferencesMissing {
            get;
            set;
        }

        public bool BuildThisProject {
            get;
            set;
        }

        public bool IsRebuilt {
            get;
            set;
        }

        public bool Building {
            get;
            set;
        }

        public string FullPath {
            get;
            set;
        }

        public string AssemblyFullPathAfterBuild {
            get;
            set;
        }

        public string AssemblyFullPathBeforeBuild {
            get;
            set;
        }

        public bool IsProjectCouldBeCompiled {
            get {
                return !IsSomeReferencesMissing && IsAllPredecessorCouldBeCompiled(this) && BuildThisProject;
            }
        }

        public Project() {
            DxProjectReferences = new List<Project>();
            DxChildProjectReferences = new List<Project>();
            IsSomeReferencesMissing = false;
            BuildThisProject = true;
            IsRebuilt = false;
            Building = false;
        }

        public bool IsAllPredecessorCouldBeCompiled(Project proj) {
            bool flag = proj.IsSomeReferencesMissing || !proj.BuildThisProject;
            bool result;
            if (flag) {
                result = false;
            } else {
                foreach (Project current in proj.DxProjectReferences) {
                    bool flag2 = !IsAllPredecessorCouldBeCompiled(current);
                    if (flag2) {
                        result = false;
                        return result;
                    }
                }
                result = true;
            }
            return result;
        }

        public bool IsAllPredecessorsAlreadyReBuilt() {
            bool result;
            foreach (Project current in DxProjectReferences) {
                bool flag = !current.IsRebuilt;
                if (flag) {
                    result = false;
                    return result;
                }
            }
            result = true;
            return result;
        }

        public override string ToString() {
            return AssemblyName + ((TargetFrameworkIdentifier == "Silverlight") ? " SL" : "");
        }
    }
}