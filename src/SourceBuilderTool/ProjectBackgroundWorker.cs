#region

using System.ComponentModel;

#endregion

namespace SourceBuilderTool {
    [ToolboxItem(false)]
    public class ProjectBackgroundWorker : BackgroundWorker {
        public Project Project {
            get;
            set;
        }

        public bool Locked {
            get;
            set;
        }
    }
}