namespace SourceBuilderTool {
    public enum ProjectCategory {
        Common,
        WinForms,
        Asp,
        Wpf,
        Silverlight,
        EXpressAppFramework,
        WinRt
    }
}