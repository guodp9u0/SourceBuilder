#region

using System.Collections.Generic;

#endregion

namespace SourceBuilderTool {
    public class ProjectComparer : IComparer<Project> {
        public int Compare(Project x, Project y) {
            bool flag = x.DxProjectReferences.Contains(y);
            int result;
            if (flag) {
                result = -1;
            } else {
                bool flag2 = y.DxProjectReferences.Contains(x);
                if (flag2) {
                    result = 1;
                } else {
                    result = 0;
                }
            }
            return result;
        }
    }
}