#region

using System.IO;

#endregion

namespace SourceBuilderTool {
    public static class ProjectFileUpdater {
        public static void ApplyPatchInternalVisibleToFiles(string file, string token) {
            string text = File.ReadAllText(file);
            string text2 = text.Replace(DevExpressKeysStrings.PublicKeyTokenName, token);
            text2 = ProjectUpdater.PatchInternalVisibleToContent(text2);
            bool flag = text2 != text;
            if (flag) {
                File.WriteAllText(file, text2);
                Logger.Instance.AddText(file + " - patched", TextItemStatus.Info);
            }
        }

        public static void ApplyDebugInfoPatch(string file) {
            string text = File.ReadAllText(file);
            bool flag = text.Contains("<DebugType>none</DebugType>");
            if (flag) {
                text = text.Replace("<DebugType>none</DebugType>", "<DebugType>pdbonly</DebugType>");
                File.WriteAllText(file, text);
                Logger.Instance.AddText(file + "- Debug config updated", TextItemStatus.Info);
            }
        }
    }
}