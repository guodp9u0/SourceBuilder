#region

using System.Collections.Generic;
using DevExpress.Utils;

#endregion

namespace SourceBuilderTool {
    public static class ProjectListManager {
        internal static void CategorizeProjects(IList<Project> loadedProjects) {
            foreach (Project current in loadedProjects) {
                bool flag = current.TargetFrameworkIdentifier == "Silverlight"
                            || current.OutputPath.Contains("Silverlight");
                if (flag) {
                    current.Category = ProjectCategory.Silverlight;
                    current.TargetFrameworkIdentifier = "Silverlight";
                } else {
                    bool flag2 = current.OutputPath.Contains("WinRT");
                    if (flag2) {
                        current.Category = ProjectCategory.WinRt;
                    } else {
                        bool flag3 = current.AssemblyName.Contains("Web");
                        if (flag3) {
                            current.Category = ProjectCategory.Asp;
                        } else {
                            bool flag4 = current.AssemblyName.Contains("Xpf");
                            if (flag4) {
                                current.Category = ProjectCategory.Wpf;
                            } else {
                                current.Category = ProjectCategory.WinForms;
                            }
                        }
                    }
                }
            }
            foreach (Project current2 in loadedProjects) {
                ProjectCategory category = current2.Category;
                foreach (Project current3 in current2.DxChildProjectReferences) {
                    bool flag5 = current3.Category != category;
                    if (flag5) {
                        current2.Category = ProjectCategory.Common;
                        break;
                    }
                }
            }
        }

        internal static void InitializeProjectReferences(IList<Project> loadedProjects) {
            foreach (Project current in loadedProjects) {
                foreach (string current2 in current.ProjectReferences) {
                    bool flag = !Exclusions.IsAssemblyToRemoveFromReferencesList(current2);
                    if (flag) {
                        Project project = FindProjectByNameAndTargetPlatform(
                            loadedProjects, current2, current.TargetFrameworkIdentifier);
                        bool flag2 = project != null;
                        if (flag2) {
                            current.DxProjectReferences.Add(project);
                        } else {
                            bool flag3 = current2.Contains("DevExpress");
                            if (flag3) {
                                current.IsSomeReferencesMissing = true;
                                Logger.Instance.AddText(
                                    string.Format(
                                        "Reference not found. Project: {0} Reference: {1} {2}", current.AssemblyName,
                                        current2, current.TargetFrameworkIdentifier), TextItemStatus.Error);
                            }
                        }
                    }
                }
            }
            foreach (Project current3 in loadedProjects) {
                current3.DxChildProjectReferences = FindProjectInReferences(loadedProjects, current3);
            }
        }

        public static IList<Project> SortItems(IList<Project> loadedProjects) {
            IComparer<Project> comparer = new ProjectComparer();
            return Algorithms.TopologicalSort(loadedProjects, comparer);
        }

        private static Project FindProjectByNameAndTargetPlatform(IList<Project> loadedProjects, string assemblyName,
            string targetFrameworkIdentifier) {
            Project result;
            foreach (Project current in loadedProjects) {
                bool flag = assemblyName.Contains("DevExpress.Design.") && current.AssemblyName.ToUpperInvariant()
                            == assemblyName.ToUpperInvariant();
                if (flag) {
                    result = current;
                    return result;
                }
                bool flag2 = current.AssemblyName.ToUpperInvariant() == assemblyName.ToUpperInvariant()
                             && current.TargetFrameworkIdentifier == targetFrameworkIdentifier;
                if (flag2) {
                    result = current;
                    return result;
                }
            }
            result = null;
            return result;
        }

        private static List<Project> FindProjectInReferences(IList<Project> loadedProjects, Project parentProject) {
            List<Project> list = new List<Project>();
            foreach (Project current in loadedProjects) {
                bool flag = current.DxProjectReferences.Contains(parentProject);
                if (flag) {
                    list.Add(current);
                }
            }
            return list;
        }
    }
}