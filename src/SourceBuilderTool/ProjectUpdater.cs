namespace SourceBuilderTool {
    public static class ProjectUpdater {
        public static string PatchInternalVisibleToContent(string fileContent) {
            return fileContent.Replace(
                DevExpressKeysStrings.PublicKeyToken, ApplicationSettings.Instance.PublicKeyToken);
        }
    }
}