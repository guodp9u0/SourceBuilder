#region

using System;
using System.IO;
using System.Reflection;

#endregion

namespace SourceBuilderTool {
    public static class ProjectsBuilder {
        public static TextItem BuildProject(Project project) {
            TextItem textItem = new TextItem {
                Status = TextItemStatus.Error
            };
            string str = string.Empty;
            string outputPath = project.OutputPath;
            string command = string.Concat(
                "\"", SystemSettings.Instance.BuildSettings.MsBuildPath,
                "\" /nologo /t:Rebuild /verbosity:quiet /p:Configuration=",
                ApplicationSettings.Instance.BuildConfiguration, ";Platform=AnyCPU;OutputPath=", outputPath, " \"",
                project.FullPath, "\"");
            str = CommandPromptOperationHelper.ExecuteCommand(
                command, ApplicationSettings.Instance.DxVersionToBuild.SourceFolder);
            bool flag = CheckIfFileExists(project, outputPath);
            if (flag) {
                project.AssemblyFullPathAfterBuild =
                    Path.GetFullPath(project.FullPath.Substring(0, project.FullPath.LastIndexOf("\\") + 1) + outputPath)
                    + project.AssemblyName + ".dll";
                textItem.Status = TextItemStatus.Success;
                textItem.Text = project.AssemblyName + " has been built successfully";
            } else {
                string text = "Error -" + project.AssemblyName + Environment.NewLine + str;
                textItem.Status = TextItemStatus.Error;
                textItem.Text = text;
            }
            return textItem;
        }

        internal static bool CheckIfFileExists(Project project, string outputRelativePath) {
            string str = Directory.GetParent(project.FullPath) + "\\";
            return File.Exists(Path.GetFullPath(str + outputRelativePath) + project.AssemblyName + ".dll");
        }

        internal static bool RegisterInGac(string assemblyFullPath) {
            bool result;
            try {
                SystemSettings.Instance.BuildSettings.Publish.GacInstall(assemblyFullPath);
            } catch (Exception var2_1B) {
                result = false;
                return result;
            }
            Assembly assembly = Assembly.ReflectionOnlyLoadFrom(assemblyFullPath);
            AssemblyName name = assembly.GetName();
            result = GacUtil.IsAssemblyInGac(
                string.Format("{0}, processorArchitecture={1}", name, name.ProcessorArchitecture));
            return result;
        }
    }
}