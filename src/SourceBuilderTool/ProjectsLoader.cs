#region

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;

#endregion

namespace SourceBuilderTool {
    public static class ProjectsLoader {
        public static void LoadProjects(IList<Project> loadedProjects) {
            Logger.Instance.ProgressValue = 0;
            ProcessDir(loadedProjects, ApplicationSettings.Instance.DxVersionToBuild.SourceFolder);
        }

        public static void ProcessDir(IList<Project> loadedProjects, string sourceDir) {
            bool flag = Directory.Exists(sourceDir);
            if (flag) {
                string[] files = Directory.GetFiles(sourceDir, "*.csproj", SearchOption.AllDirectories);
                Logger.Instance.MaxProgressValue = files.Length;
                string[] array = files;
                for (int i = 0; i < array.Length; i++) {
                    string fileName = array[i];
                    Logger.Instance.ProgressValue = Logger.Instance.ProgressValue + 1;
                    ParseProject(fileName, loadedProjects);
                }
                Logger.Instance.ProgressValue = 0;
            } else {
                Logger.Instance.AddText("Folder not found...", TextItemStatus.Error);
            }
        }

        private static void ParseProject(string fileName, IList<Project> loadedProjects) {
            Project project = new Project();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            project.AssemblyName = FindElement(xmlDocument, "AssemblyName");
            project.OutputPath = FindElement(xmlDocument, "OutputPath");
            project.TargetFrameworkVersion = FindElement(xmlDocument, "TargetFrameworkVersion");
            project.TargetFrameworkIdentifier = GetIdentificator(
                FindElement(xmlDocument, "TargetFrameworkIdentifier"), project.OutputPath);
            project.ProjectReferences = FindElements(xmlDocument, fileName, "Reference");
            project.FullPath = fileName;
            loadedProjects.Add(project);
        }

        public static void DetectCrossReferences(IList<Project> loadedProjects) {
            List<string> list = new List<string>();
            foreach (Project current in loadedProjects) {
                List<Project> list2 = new List<Project>();
                CollectAllRef(current, list2);
                bool flag = list2.Contains(current);
                if (flag) {
                    list.Add("A Cross Reference is found in references of:" + current.AssemblyName);
                }
            }
            string[] array = list.ToArray();
            bool flag2 = array.Length != 0;
            if (flag2) {
                string[] array2 = array;
                for (int i = 0; i < array2.Length; i++) {
                    string text = array2[i];
                    Logger.Instance.AddText(text, TextItemStatus.Error);
                }
                ApplicationWorkflow.Instance.IsBuildPossible = false;
                ApplicationWorkflow.Instance.BuildInProgress = false;
            }
        }

        private static void CollectAllRef(Project mainProject, List<Project> allChildReferences) {
            foreach (Project current in mainProject.DxProjectReferences) {
                bool flag = !allChildReferences.Contains(current);
                if (flag) {
                    allChildReferences.Add(current);
                    CollectAllRef(current, allChildReferences);
                }
            }
        }

        private static string GetIdentificator(string targetFrameworkIdentifier, string outputPath) {
            bool flag = string.IsNullOrEmpty(targetFrameworkIdentifier);
            string result;
            if (flag) {
                bool flag2 = outputPath.Contains("Silverlight");
                if (flag2) {
                    result = "Silverlight";
                    return result;
                }
                bool flag3 = outputPath.Contains("WinRT");
                if (flag3) {
                    result = "WinRT";
                    return result;
                }
            }
            result = targetFrameworkIdentifier;
            return result;
        }

        private static string FindElement(XmlDocument doc, string key) {
            XmlNodeList elementsByTagName = doc.GetElementsByTagName(key);
            bool flag = elementsByTagName.Count > 0;
            string result;
            if (flag) {
                result = elementsByTagName[0].InnerText;
            } else {
                result = string.Empty;
            }
            return result;
        }

        private static List<string> FindElements(XmlDocument doc, string fileName, string key) {
            XmlNodeList elementsByTagName = doc.GetElementsByTagName(key);
            List<string> list = new List<string>();
            bool flag = elementsByTagName.Count > 0;
            List<string> result;
            if (flag) {
                foreach (XmlNode xmlNode in elementsByTagName) {
                    bool flag2 = xmlNode.Attributes[0].Value != null;
                    if (flag2) {
                        list.Add(ClearFullReference(xmlNode.Attributes[0].Value));
                        bool flag3 = xmlNode.Attributes[0].Value.Contains("System.Web.Mvc");
                        if (flag3) {
                            Assembly assembly = null;
                            try {
                                assembly = Assembly.ReflectionOnlyLoad(xmlNode.Attributes[0].Value);
                            } catch {
                            }
                            bool flag4 = assembly != null;
                            if (flag4) {
                                string text = assembly.FullName + ", processorArchitecture=MSIL";
                                bool flag5 = xmlNode.Attributes[0].Value != text;
                                if (flag5) {
                                    xmlNode.Attributes[0].Value = text;
                                    doc.Save(fileName);
                                }
                            }
                        }
                        bool flag6 = xmlNode.Attributes[0]
                            .Value.Contains("Microsoft.VisualStudio.Shell, Version=2.0.0.0");
                        if (flag6) {
                            xmlNode.Attributes[0].Value =
                                "Microsoft.VisualStudio.Shell, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, processorArchitecture=MSIL";
                            doc.Save(fileName);
                        }
                    }
                }
                result = list;
            } else {
                result = list;
            }
            return result;
        }

        private static string ClearFullReference(string p) {
            bool flag = p.Contains(',');
            string result;
            if (flag) {
                bool flag2 = p.Contains("DevExpress");
                if (flag2) {
                    result = p.Substring(0, p.IndexOf(','));
                    return result;
                }
            }
            result = p;
            return result;
        }

        internal static void RemoveAssemblies(IList<Project> loadedProjects) {
            for (int i = 0; i < loadedProjects.Count; i++) {
                bool flag = Exclusions.IsAssemblyToRemoveFromBuildList(loadedProjects[i].AssemblyName)
                            || Exclusions.IsUwpProject(loadedProjects[i])
                            || Exclusions.IsEmbeddedTemplateProject(loadedProjects[i])
                            || ContainsBuildToIgnore(loadedProjects[i]);
                if (flag) {
                    loadedProjects.RemoveAt(i);
                    i--;
                }
            }
        }

        private static bool ContainsBuildToIgnore(Project project) {
            bool result;
            foreach (string current in ApplicationSettings.Instance.BuildIgnoreProjects) {
                bool flag = project.AssemblyName.Contains(current);
                if (flag) {
                    result = true;
                    return result;
                }
            }
            result = false;
            return result;
        }
    }
}