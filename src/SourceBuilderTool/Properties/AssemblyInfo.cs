#region

using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#endregion

[assembly: AssemblyVersion("1.0.5.0")]
[assembly:
    Debuggable(
        DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations
        | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints
        | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © 2017")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.5")]
[assembly: AssemblyProduct("SourceBuilderTool")]
[assembly: AssemblyTitle("SourceBuilderTool")]
[assembly: AssemblyTrademark("")]
[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: ComVisible(false)]
[assembly: Guid("99962703-6843-4737-93c4-a91b3ddbeee7")]