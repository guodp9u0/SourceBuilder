#region

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

#endregion

namespace SourceBuilderTool.Properties {
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode,
     CompilerGenerated]
    internal class Resources {
        private static ResourceManager _resourceMan;

        private static CultureInfo _resourceCulture;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static ResourceManager ResourceManager {
            get {
                bool flag = _resourceMan == null;
                if (flag) {
                    ResourceManager resourceManager = new ResourceManager(
                        "SourceBuilderTool.Properties.Resources", typeof (Resources).Assembly);
                    _resourceMan = resourceManager;
                }
                return _resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }

    }
}