#region

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

#endregion

namespace SourceBuilderTool.Properties {
    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"),
     CompilerGenerated]
    internal sealed class Settings : ApplicationSettingsBase {
        private static Settings _defaultInstance = (Settings) Synchronized(new Settings());

        public static Settings Default {
            get {
                return _defaultInstance;
            }
        }
    }
}