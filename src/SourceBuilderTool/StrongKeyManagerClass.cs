#region

using System;
using System.IO;
using System.Windows.Forms;

#endregion

namespace SourceBuilderTool {
    public static class StrongKeyManagerClass {
        public static bool IsKeyExists() {
            return File.Exists(GetSnKeyPath());
        }

        public static void GenerateRandomKey(string sourceFolder, string snPath) {
            string str = BuildKeyFile(sourceFolder, snPath);
            Logger.Instance.AddText("The StongKey file has been generated successfully" + str, TextItemStatus.Info);
        }

        public static bool CopyKey() {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = StrongKeyManagerClassStrings.SnkOpenFileDialogFilter;
            bool flag = openFileDialog.ShowDialog() != DialogResult.OK;
            bool result;
            if (flag) {
                result = false;
            } else {
                bool flag2 = !File.Exists(openFileDialog.FileName);
                if (flag2) {
                    result = false;
                } else {
                    File.Copy(openFileDialog.FileName, GetSnKeyPath());
                    ApplicationSettings.Instance.KeyTokenPath = GetSnKeyPath();
                    result = true;
                }
            }
            return result;
        }

        private static string BuildKeyFile(string sourceFolder, string snPath) {
            string result = string.Empty;
            string sNKeyPath = GetSnKeyPath();
            string command = string.Concat("\"", snPath, "\" -k \"", sNKeyPath, "\"");
            string text = CommandPromptOperationHelper.ExecuteCommand(command, sourceFolder);
            bool flag = File.Exists(sNKeyPath);
            if (flag) {
                result = string.Format("{0}{1}", StrongKeyManagerClassStrings.KeyGeneratedLogMsg, Environment.NewLine);
                ApplicationSettings.Instance.KeyTokenPath = GetSnKeyPath();
            } else {
                Logger.Instance.AddText("Error during generating strong key file", TextItemStatus.Error);
            }
            return result;
        }

        private static string GetSnKeyPath() {
            return Path.Combine(
                ApplicationSettings.Instance.DxVersionToBuild.SourceFolder,
                StrongKeyManagerClassStrings.SnKeyDirectoryName, StrongKeyManagerClassStrings.SnKeyFileName);
        }
    }
}