namespace SourceBuilderTool {
    public static class StrongKeyManagerClassStrings {
        public static readonly string SnkOpenFileDialogFilter = "snk files (*.snk)|*.snk|all files (*.*)|*.*";

        public static readonly string SnKeyDirectoryName = "DevExpress.Key";

        public static readonly string SnKeyFileName = "StrongKey.snk";

        public static readonly string KeyGeneratedLogMsg =
            "The new KeyToken is generated in the \\DevExpress.Key folder";
    }
}