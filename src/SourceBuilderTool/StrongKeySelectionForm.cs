#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace SourceBuilderTool {
    public class StrongKeySelectionForm : Form {
        private IContainer components = null;

        private LinkLabel _linkLabel1;

        private Label _label1;

        private Button _button2;

        private Button _button1;

        public string Operation {
            get;
            set;
        }

        public StrongKeySelectionForm() {
            InitializeComponent();
            Operation = "";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start("http://www.devexpress.com/Support/Center/Question/Details/A609");
        }

        private void button1_Click(object sender, EventArgs e) {
            Operation = "random";
            Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            Operation = "copy";
            Close();
        }

        protected override void Dispose(bool disposing) {
            bool flag = disposing && components != null;
            if (flag) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent() {
            _linkLabel1 = new LinkLabel();
            _label1 = new Label();
            _button2 = new Button();
            _button1 = new Button();
            SuspendLayout();
            _linkLabel1.AutoSize = true;
            _linkLabel1.Font = new Font("Segoe UI", 11.25f);
            _linkLabel1.Location = new Point(12, 29);
            _linkLabel1.Name = "_linkLabel1";
            _linkLabel1.Size = new Size(203, 20);
            _linkLabel1.TabIndex = 9;
            _linkLabel1.TabStop = true;
            _linkLabel1.Text = "Learn more about StrongKey";
            _linkLabel1.LinkClicked += linkLabel1_LinkClicked;
            _label1.AutoSize = true;
            _label1.Font = new Font("Segoe UI", 11.25f);
            _label1.Location = new Point(12, 9);
            _label1.Name = "_label1";
            _label1.Size = new Size(424, 20);
            _label1.TabIndex = 7;
            _label1.Text = "The StrongKey file was not found in the DevExpress.Key Folder.";
            _button2.Font = new Font("Segoe UI", 11.25f);
            _button2.Location = new Point(384, 110);
            _button2.Name = "_button2";
            _button2.Size = new Size(176, 41);
            _button2.TabIndex = 6;
            _button2.Text = "Select an existing";
            _button2.UseVisualStyleBackColor = true;
            _button2.Click += button2_Click;
            _button1.Font = new Font("Segoe UI", 11.25f);
            _button1.Location = new Point(202, 110);
            _button1.Name = "_button1";
            _button1.Size = new Size(176, 41);
            _button1.TabIndex = 5;
            _button1.Text = "Generate a random one";
            _button1.UseVisualStyleBackColor = true;
            _button1.Click += button1_Click;
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(572, 163);
            ControlBox = false;
            Controls.Add(_linkLabel1);
            Controls.Add(_label1);
            Controls.Add(_button2);
            Controls.Add(_button1);
            Font = new Font("Segoe UI", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 204);
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "StrongKeySelectionForm";
            ShowIcon = false;
            Text = "StrongKey Selection Form";
            ResumeLayout(false);
            PerformLayout();
        }
    }
}