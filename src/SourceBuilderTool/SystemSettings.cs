#region

using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using Microsoft.Win32;

#endregion

namespace SourceBuilderTool {
    public class SystemSettings {
        private static SystemSettings _instance;

        public static SystemSettings Instance {
            get {
                bool flag = _instance == null;
                if (flag) {
                    _instance = new SystemSettings();
                }
                return _instance;
            }
        }

        public SystemToolsLocation BuildSettings {
            get;
            set;
        }

        public string WinDir {
            get;
            set;
        }

        public string ProgramFilesFolder {
            get;
            set;
        }

        public Collection<Dxperience> DxVersionsInstalled {
            get;
            set;
        }

        public string GacFullPath {
            get {
                return WinDir + "\\assembly\\GAC_MSIL\\";
            }
        }

        public string Gac4FullPath {
            get {
                return WinDir + "\\Microsoft.NET\\assembly\\GAC_MSIL\\";
            }
        }

        public SystemSettings() {
            WinDir = Environment.GetFolderPath(Environment.SpecialFolder.Windows);
            ProgramFilesFolder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            GetDxVersionsInstalled();
            BuildSettings = new SystemToolsLocation(this);
        }

        private void GetDxVersionsInstalled() {
            DxVersionsInstalled = new Collection<Dxperience>();
            RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            RegistryKey registryKey2 = registryKey.OpenSubKey("Software\\DevExpress\\\\", false);
            bool flag = registryKey2 != null;
            if (flag) {
                RegistryKey registryKey3 = registryKey2.OpenSubKey("DXperience");
                bool flag2 = registryKey3 != null;
                if (flag2) {
                    FillDxVersionsTable(registryKey3);
                }
                registryKey3 = registryKey2.OpenSubKey("Components");
                bool flag3 = registryKey3 != null;
                if (flag3) {
                    FillDxVersionsTable(registryKey3);
                }
            } else {
                Logger.Instance.AddText("Error in registry. DevExpress key was not found", TextItemStatus.Error);
            }
        }

        private void FillDxVersionsTable(RegistryKey key) {
            string[] subKeyNames = key.GetSubKeyNames();
            string[] array = subKeyNames;
            for (int i = 0; i < array.Length; i++) {
                string text = array[i];
                string text2 = key.OpenSubKey(text).GetValue("RootDirectory").ToString();
                bool flag = Directory.Exists(text2 + "Sources\\DevExpress.Data\\");
                if (flag) {
                    string sourceFolder = text2 + "Sources\\";
                    double num = Convert.ToDouble(text.Substring(1), CultureInfo.InvariantCulture);
                    bool flag2 = num > 12.1;
                    if (flag2) {
                        DxVersionsInstalled.Add(
                            new Dxperience {
                                Version = text,
                                RootFolder = text2,
                                SourceFolder = sourceFolder
                            });
                    }
                } else {
                    bool flag3 = Directory.Exists(text2 + "\\Components\\Sources\\DevExpress.Data\\");
                    if (flag3) {
                        string sourceFolder = text2 + "Components\\Sources\\";
                        double num2 = Convert.ToDouble(text.Substring(1), CultureInfo.InvariantCulture);
                        bool flag4 = num2 > 12.1;
                        if (flag4) {
                            DxVersionsInstalled.Add(
                                new Dxperience {
                                    Version = text,
                                    RootFolder = text2,
                                    SourceFolder = sourceFolder
                                });
                        }
                    }
                }
            }
        }
    }
}