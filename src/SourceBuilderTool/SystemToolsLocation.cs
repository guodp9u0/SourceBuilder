#region

using System.EnterpriseServices.Internal;
using System.IO;
using Microsoft.Build.Utilities;

#endregion

namespace SourceBuilderTool {
    public class SystemToolsLocation {
        public string SnPath {
            get;
            private set;
        }

        public string GacUtilPath {
            get;
            private set;
        }

        public string MsBuildPath {
            get;
            private set;
        }

        public Publish Publish {
            get;
            private set;
        }

        public SystemToolsLocation(SystemSettings systemSettings) {
            SnPath = ToolLocationHelper.GetPathToDotNetFrameworkSdkFile(
                "sn.exe", TargetDotNetFrameworkVersion.Version40);
            GacUtilPath = ToolLocationHelper.GetPathToDotNetFrameworkSdkFile(
                "gacutil.exe", TargetDotNetFrameworkVersion.Version40);
            MsBuildPath = ToolLocationHelper.GetPathToDotNetFrameworkFile(
                "MSBuild.exe", TargetDotNetFrameworkVersion.Version40);
            Publish = new Publish();
        }

        private static string GetPath(string basePath, string mainPath, string alternativePath) {
            bool flag = File.Exists(basePath + mainPath);
            string result;
            if (flag) {
                result = basePath + mainPath;
            } else {
                bool flag2 = File.Exists(basePath + alternativePath);
                if (flag2) {
                    result = basePath + alternativePath;
                } else {
                    result = null;
                }
            }
            return result;
        }
    }
}