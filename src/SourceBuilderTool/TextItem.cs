namespace SourceBuilderTool {
    public class TextItem {
        public int Index {
            get;
            set;
        }

        public string Text {
            get;
            set;
        }

        public TextItemStatus Status {
            get;
            set;
        }
    }
}