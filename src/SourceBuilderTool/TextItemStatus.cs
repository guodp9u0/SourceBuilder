namespace SourceBuilderTool {
    public enum TextItemStatus {
        Info,
        Warning,
        Error,
        Success
    }
}