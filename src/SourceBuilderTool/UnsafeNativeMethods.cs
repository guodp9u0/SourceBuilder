#region

using System;
using System.Runtime.InteropServices;

#endregion

namespace SourceBuilderTool {
    public static class UnsafeNativeMethods {
        public struct Cpinfoexw {
            public int MaxCharSize;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public byte[] DefaultChar;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public byte[] LeadByte;

            public short UnicodeDefaultChar;

            public int CodePage;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
            public short[] CodePageName;
        }

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        private static extern int GetCPInfoExW(int codePage, int dwFlags, out Cpinfoexw lpCpInfoEx);

        public static Cpinfoexw GetCpInfoExWSafe() {
            Cpinfoexw result;
            bool flag = GetCPInfoExW(1, 0, out result) == 0;
            if (flag) {
                throw new InvalidOperationException(
                    "The GetCPInfoEx function returns error code " + Marshal.GetLastWin32Error());
            }
            return result;
        }
    }
}